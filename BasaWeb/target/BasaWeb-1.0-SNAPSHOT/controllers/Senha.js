var basaControllers = angular.module('autentica');

basaControllers.controller('Senha', ['$scope', '$http',
    function ($scope, $http) {
        $scope.regInfo = true;

        $scope.clickBtn = function ()
        {
            $scope.regInfo = true;
        };
        
        try{
            $scope.recuperarSenha = function (){
            var email = $scope.email;
            var senha = $scope.senha;
            
            if(email !== undefined && senha !== undefined){
                $scope.autInfo = true;

                var fd = new FormData();
                fd.append("email", email);
                fd.append("novaSenha", senha);
                console.log(email)
                console.log(senha)
                $http.post(rootPath + "Autenticacao/recuperarSenha", fd, {headers: {'Content-Type': undefined}})
                        .success(function (resp){
                            if (resp.code) {
                                $scope.errMsg = resp.msg;
                                $scope.autInfo = false;
                                return;
                            
                            }
                            $scope.Msg = "Você já pode fazer login com esse email e nova senha.";
                        }).error(function (response){
                        $scope.errMsg = "Falha ao conectar com o servidor, verifique sua conxão com a internet. Caso sua internet estaja ok, os administradores do sistema já foram notificados do problema ocorrido.";
                                $scope.autInfo = false;
                                throw JSON.stringify(response);    
                        
                    });
            }
            else if(email === undefined){
                $scope.errMsg = 'O campo "Email" deve ser preenchido.';
                $scope.autInfo = false;
        }
        else if(senha === undefined){
            $scope.errMsg = 'O campo "Senha" deve ser preenchido.';
            $scope.autInfo = false;
        }
        
    };
            
        }
        catch (e){
            var fd = new FormData();
            var timestamp = new Date().getTime();
            var msg = "Erro em rotina de recuoerar senha.";
            var stacktrace = e.stack;

            fd.append("timestamp", timestamp);
            fd.append("msg", msg);
            fd.append("stacktrace", stacktrace);
        }
    
    }]);
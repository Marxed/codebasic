var basaControllers = angular.module('autentica');

basaControllers.controller('Cadastro', ['$scope', '$http',
    function ($scope, $http) {
        $scope.regInfo = true;

        $scope.usuario = {};

        $scope.clickBtn = function ()
        {
            $scope.regInfo = true;
        };

        try
        {
            $scope.register = function ()
            {
                var userName = $scope.usuario.nome;
                var userMail = $scope.usuario.email;
                var userPassword = $scope.usuario.senha;
                var passwordCfrm = $scope.usuario.confsenha;

                
                if (userName !== undefined && userMail !== undefined && userPassword !== undefined && passwordCfrm !== undefined)
                {
                    $scope.regInfo = true;
                   
                    if (userPassword !== passwordCfrm) {
                        $scope.errMsg = 'Senhas diferentes! Senha e Confirm. Senha devem ser iguais.';
                        alert($scope.errMsg);
                        $scope.regInfo = false;
                        return null;
                    }


                    var fd = new FormData();
                    fd.append("nome", userName);
                    fd.append("email", userMail);
                    fd.append("senha", userPassword);
     

                    $http.post(rootPath + "Usuario/salvar", fd, {headers: {'Content-Type': undefined}})
                            .success(function (resp)
                            {
                    
                                if (resp.code) {
                                    $scope.errMsg = resp.msg;
                                    $scope.regInfo = false;
                                    return;
                                }
                                var usuario = {};

                                usuario.idUsuario = resp.idUsuario;
                                usuario.nome = $scope.name;
                                usuario.login = $scope.login;
                                sessionStorage.usuario = JSON.stringify(usuario);
                                location.href = "Home.html";
                                
                                $scope.Msg = "Usuário cadastrado com sucesso. Faça seu login";
                               

                            })
                            .error(function (response)
                            {
                                $scope.errMsg = "Falha ao conectar com o servidor, verifique sua conxão com a internet. Caso sua internet estaja ok, os administradores do sistema já foram notificados do problema ocorrido.";
                                $scope.autInfo = false;
                                throw JSON.stringify(response);
                            });

                } else if (userName === undefined) {
                    $scope.errMsg = 'O campo "Nome" deve ser preenchido.';
                    $scope.regInfo = false;
                } else if (userMail === undefined)
                {
                    $scope.errMsg = 'O campo "E-mail" deve ser preenchido.';
                    $scope.regInfo = false;
                } else if (userPassword === undefined)
                {
                    $scope.errMsg = 'O campo "Senha" deve ser preenchido.';
                    $scope.regInfo = false;
                } else
                {
                    $scope.errMsg = 'O campo "Confirm. Senha" deve ser preenchido.';
                    $scope.regInfo = false;
                }
            };



        } catch (e)
        {
            
            var fd = new FormData();
            var timestamp = new Date().getTime();
            var msg = "Erro em rotina de cadastro de usuario";
            var stacktrace = e.stack;
            
            fd.append("timestamp", timestamp);
            fd.append("msg", msg);
            fd.append("stacktrace", stacktrace);
            
            $http.post(rootPath + "Notificacao/email", fd, {headers: {'Content-Type': undefined}})
                    .success(function (resp){
                        
                        
            });
            

        }

    }]);

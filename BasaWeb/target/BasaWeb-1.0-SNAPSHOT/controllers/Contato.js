var basaControllers = angular.module('autentica');
basaControllers.controller('Contato', ['$scope', '$http',
        function ($scope, $http){
        $scope.regInfo = true;

        $scope.clickBtn = function ()
        {
            $scope.regInfo = true;
        };
        
        try{
            
            $scope.contact = function (){
                var nome = $scope.nome;
                var email = $scope.email;
                var mensagem = $scope.mensagem;
                
                if(nome !== undefined &&email !== undefined && mensagem !== undefined){
                    $scope.autInfo = true;
                   
                    var fd = new FormData();
                    fd.append("nome", nome);
                    fd.append("email", email);
                    fd.append("msg", mensagem);
                    
                    console.log(nome);
                    console.log(email);
                    console.log(mensagem);
                    $http.post(rootPath + "Notificacao/contato", fd, {headers: {'Content-Type': undefined}})
                            .success(function (resp)
                            {
                                if (resp.code) {
                                    $scope.errMsg = resp.msg;
                                    $scope.autInfo = false;
                                    return;
                                }
                                
                                $scope.men = "Email enviado com sucesso.";
                                console.log($scope.men);
                               
                            })
                            .error(function(response){
                                $scope.errMsg = "Falha ao conectar com o servidor, verifique sua conxão com a internet. Caso sua internet estaja ok, os administradores do sistema já foram notificados do problema ocorrido.";
                                $scope.autInfo = false;
                                throw JSON.stringify(response);
                            });
                }
                
            };
        }
        catch (e)
        {
            var fd = new FormData();
            var timestamp = new Date().getTime();
            var msg = "Erro em rotina de contato.";
            var stacktrace = e.stack;

            fd.append("timestamp", timestamp);
            fd.append("msg", msg);
            fd.append("stacktrace", stacktrace);
        }
    }]);
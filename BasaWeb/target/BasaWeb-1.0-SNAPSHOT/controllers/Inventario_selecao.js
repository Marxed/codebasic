var basaControllers = angular.module('basApp');

basaControllers.controller('Inventario_selecao', ['$scope','$http','$location','passaInventario','returnresponse',
    function ($scope, $http, $location,passaInventario, returnresponse) {
        $scope.edit = false;
        
        $scope.row = {};
        
        $scope.init = function(){
           
           if(returnresponse.get() == "0"){
               $('#myAlert').show();
               $scope.Msg = "Seu inventário foi salvo com sucesso";
           }
        };
        
        function prepareData(data) {
            var fd = new FormData();
            for (var key in data) {
                fd.append(key, data[key]);
            }
            return fd;
        }
        
        $scope.ep = {};
        $scope.usuario = JSON.parse(sessionStorage.usuario);
        var fd = new FormData();
        fd.append("usuarioId", $scope.usuario.idUsuario);
        
        $scope.getAll = function (){
            $("#myModal").modal('show');
            var tabelaUrl = rootPath + "InventarioSelecao/consultar";
           
            var req = {
            method: 'POST',
            url: tabelaUrl,
            headers: {
              'Content-Type': undefined
            },
            data: fd
           };

            $http(req).then(function(response){
           
                $scope.ep= response.data.tabelaInventarios;
                $("#myModal").modal('hide');
            }, function(){
           
                $("#myModal").modal('hide');
            });
        };
        
        
        
        //edit table
        $scope.edittable = function(e){
            
           $scope.ep.selected =  angular.copy(e);
           console.log($scope.ep.selected);
      
        };
        
        $scope.plc = function(e){
            passaInventario.set(e.inventarioId);
            $location.path("/Inventario-Consulta");
        };
    
    $scope.reset = function () {
          $scope.ep.selected = {};
    };
        
        // delete table
        $scope.deleterow = function(e){
            $("#myModal").modal('show');
            var fd = new FormData();
            fd.append("inventarioId", e.inventarioId);
            $http.post(rootPath + "InventarioSelecao/delete",fd,{headers:{'Content-Type': undefined}})
                    .success(function (response){
                        var index = $scope.ep.indexOf(e);
                        $scope.ep.splice(index, 1);
                        $("#myModal").modal('hide');
            });
           
        };
            
        
    }]);
        
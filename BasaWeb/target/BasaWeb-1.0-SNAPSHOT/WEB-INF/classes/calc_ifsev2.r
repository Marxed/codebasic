# ##Versao final

args = commandArgs(trailingOnly=TRUE)

rootPath <-  args[1]

if( "jsonlite" %in% installed.packages() == FALSE){
              install.packages('jsonlite')
         }

library(RMySQL)
library(psych)

if( "RMySQL" %in% installed.packages() == FALSE){
         install.packages('RMySQL')
     }

library(jsonlite)

#all.equal(mtcars, fromJSON(toJSON(mtcars)))
singleString <- paste(readLines(args[2]))
json <- singleString
mydf <- fromJSON(json)
mydf <- as.data.frame(mydf)

qtd_linhas <- nrow(mydf)

tabela = matrix(0, qtd_linhas, 9)
tabela[1:qtd_linhas, 1] = mydf[1:qtd_linhas, 1]
tabela[1:qtd_linhas, 2] = mydf[1:qtd_linhas, 2]
tabela[1:qtd_linhas, 3] = mydf[1:qtd_linhas, 3]

tabela[1:qtd_linhas, 4] = mydf[1:qtd_linhas, 4]
tabela[1:qtd_linhas, 5] = mydf[1:qtd_linhas, 7]
tabela[1:qtd_linhas, 6] = mydf[1:qtd_linhas, 5]

tabela[1:qtd_linhas, 7] = mydf[1:qtd_linhas, 8]
tabela[1:qtd_linhas, 8] = mydf[1:qtd_linhas, 9]
tabela[1:qtd_linhas, 9] = mydf[1:qtd_linhas, 10]

inventario = as.data.frame(tabela)
col_headings_inventario <- c('nome_cientifico','nome_popular','familia','num_individuos','freq_absoluta','area_basal','valor_comercial','biomassa','usos')
names(inventario) <- col_headings_inventario

 inventario$nome_cientifico = as.character(inventario$nome_cientifico)
 inventario$nome_popular = as.character(inventario$nome_popular)
 inventario$familia = as.character(inventario$familia)
 inventario$num_individuos = as.character(inventario$num_individuos)
 inventario$num_individuos = as.numeric(inventario$num_individuos)
 inventario$freq_absoluta = as.character(inventario$freq_absoluta)
 inventario$freq_absoluta = as.numeric(inventario$freq_absoluta)
 inventario$area_basal = as.character(inventario$area_basal)
 inventario$area_basal = as.numeric(inventario$area_basal)
 inventario$valor_comercial = as.character(inventario$valor_comercial)
 inventario$valor_comercial = as.numeric(inventario$valor_comercial)
 inventario$biomassa = as.character(inventario$biomassa)
 inventario$biomassa = as.numeric(inventario$biomassa)
 inventario$usos = as.character(inventario$usos)
 inventario$usos = as.numeric(inventario$usos)

tabela_completa = matrix(0, qtd_linhas, 9)  

tabela_completa = data.frame(tabela_completa)

col_headings <- c('nome_cientifico','nome_popular','familia','abr','fqr','dor','vcmr','biomr','usor')
names(tabela_completa) <- col_headings

tabela_completa[,1:3] <- inventario[,1:3]

#abr - densidade relativa

total_individuos = sum(inventario$num_individuos)#[1:qtd_linhas,4])

for (a in 1:qtd_linhas) {
  
  tabela_completa[a:qtd_linhas,4] = round((inventario[a,4]/total_individuos)*100, digits = 5)
  
}

#fqr - frequencia relativa

total_frequencia = sum(inventario$freq_absoluta)

for (f in 1:qtd_linhas) {
  
  tabela_completa[f:qtd_linhas,5] = round((inventario[f,5]/total_frequencia)*100, digits = 5)
  
}

#dor - dominancia relativaï¿½

total_area_basal = sum(inventario$area_basal)

for (d in 1:qtd_linhas){
  
  tabela_completa[d:qtd_linhas,6] = round((inventario[d,6]/total_area_basal)*100, digits = 5)
}  


#vcmr - valor comercial da madeira

valor_total = sum(inventario$valor_comercial)

for(v in 1:qtd_linhas){
  
  tabela_completa[v:qtd_linhas,7] = round((inventario[v,7]/valor_total)*100, digits = 5)
}

#biomr - biomassa

total_biom = sum(inventario$biomassa)

for (b in 1:qtd_linhas) {
  
  tabela_completa[b:qtd_linhas,8] = round((inventario[b,8]/total_biom)*100, digits = 5)
  
}

#usor - usos

total_usos = sum(inventario$usos)

for (t in 1:qtd_linhas) {
  
  tabela_completa[t:qtd_linhas,9] = round((inventario[t,9]/total_usos)*100, digits = 5)
  
}

#Script calculo IFSE
#autor: Beatriz Nery e Markus

#OBS: posteriormente sera criada uma variavel que represente a criacao de matriz, bem como a quantidade de
#suas linhas e colunas

##############1Ã¯Â¿Â½Parte################tabelas##############################################
#carregar tabela (data set)                                                                         #
#tabela_completa = read.csv2('tabela_ifse.csv', header = TRUE, sep = ';', dec = ',')  

qtd_linhas <- nrow(tabela_completa) #qtd de linhas do arquivo .csv

qtd_colunas <- ncol(tabela_completa) #qtd de colunas


#criar tabela de 0's para posteriormente colocar o index. 
tabela_ranqueada_abr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_fqr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_dor = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_biomr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_vcmr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_usor = matrix(0, qtd_linhas, qtd_colunas)

tabela_dummy = matrix(0,qtd_linhas,1) #tabela para colocar as colunas com as variÃƒÂ¡veis dummy

#ordena todas as colunas a partir da coluna abr
index_abr<- with(tabela_completa, order(abr,fqr, dor, vcmr, biomr, usor, decreasing = TRUE))
tabela_ranqueada_abr =  tabela_completa[index_abr,]   
#ordena todas as colunas a partir de fqr
index_fqr <- with(tabela_completa, order(fqr, abr, dor, vcmr, biomr, usor, decreasing = TRUE))
tabela_ranqueada_fqr = tabela_completa[index_fqr,]
#ordena todas as colunas a partir da coluna dor
index_dor <- with(tabela_completa, order(dor, abr, fqr, vcmr, biomr, usor, decreasing = TRUE))
tabela_ranqueada_dor = tabela_completa[index_dor,]
#ordena todas as colunas a partir da coluna vcmr
index_vcmr <- with(tabela_completa, order(vcmr, abr, fqr, dor, biomr, usor, decreasing = TRUE))
tabela_ranqueada_vcmr = tabela_completa[index_vcmr,]
#ordena todas as colunas a partir da coluna biomr
index_biomr <- with(tabela_completa, order(biomr, abr, fqr, dor, vcmr, usor, decreasing = TRUE))
tabela_ranqueada_biomr = tabela_completa[index_biomr,]
#ordena 
index_usor <- with(tabela_completa, order(usor, abr, fqr, dor, vcmr, biomr, decreasing = TRUE))
tabela_ranqueada_usor = tabela_completa[index_usor,]

##############################Calculo ###############################################
t_individuos <- total_individuos# 36298 #nÂº total de indiviuos em mil

t_especies <- qtd_linhas #898 #n? total de especies

t_parcelas_especies <- 315 #total de parcelas de ocorrencia pra todas as especies

t_area_basal <- total_area_basal

t_biomassa_especies <- total_biom  #26377324.25  #biomassa de todas as especies

num_plantar <- 1667

valor_total_comercial_madeira <- valor_total #vcmr total em R$ Obs: esse valor esta na tabela do spss(tabelÃƒÂ£o)
t_uso_especie <- total_usos #203 #usor da especie

############dummy#############
#Tabela que irÃƒÂ¡ armazenar as variaveis e suas respectivas variaveis dummys.
tabela_completaD = matrix(0,qtd_linhas,12)

#abr#
for (i in qtd_linhas) {
  
  n_individuos_especie_abr <- round((tabela_ranqueada_abr$abr/100) * t_individuos, digits = 5) #
}

dim(n_individuos_especie_abr) <- c(qtd_linhas,1) #transforma um vetor em matriz

soma <- 0 #inicializa a variÃƒÂ¡vel soma com 0
x <- matrix(0,qtd_linhas,1) #cria um matriz de 0's com uma coluna e 898 linhas #obs.
#for para criacao das variaveis dummy
for (a in 1:qtd_linhas) {
  
  teste <- sum(n_individuos_especie_abr[a,]) #valor da linha da matriz
  
  soma <- round(soma+teste, digits = 5) #soma as linhas da matriz
  
  if (soma <= (t_individuos/2)) {
    
    x[a,1] <- 1 #escrever variÃƒÂ¡vei dummy na matriz
  }
}
index_F = index_abr;
tabela_apoio = matrix(0, qtd_linhas,2)
tabela_apoio[,1] = index_abr
tabela_apoio[,2] = x

caminhoCol <- "colDummy.R"
caminhoColD <- paste(rootPath,caminhoCol,sep ="")

source(caminhoColD)

tabela_completaD[,1] = tabela_completa[,4]
tabela_completaD[,2] = tabela_dummy


#fqr#
#pegar o numero de parcelas por especie
for(f in qtd_linhas){
  
  n_parcelas_especie_fqr <- round((tabela_ranqueada_fqr$fqr/100) * t_parcelas_especies, digits = 5) #obter o valor de n_parcelas
}

dim(n_parcelas_especie_fqr) <- c(qtd_linhas,1) #transforma um vetor em matriz

soma_fqr <- 0 #inicializa a variÃƒÂ¡vel com 0
x_fqr <- matrix(0, qtd_linhas, 1) #cria um matriz de 0's com uma coluna e 898 linhas #obs.
#for para somar fqr e escrever coluna 
for(fq in 1:qtd_linhas){
  teste_fqr <- sum(n_parcelas_especie_fqr[fq,]) #pegar valor de cada linha da matriz
  
  soma_fqr <- round(soma_fqr + teste_fqr, digits = 5) #soma o valor das linhas da coluna
  if (soma_fqr <= (t_parcelas_especies/2)){
    #criar matriz p/ colocar dummy
    x_fqr[fq,1] <- 1 #escrever variÃƒÂ¡vei dummy na matriz
  }
}

index_F = index_fqr;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_fqr
source(caminhoColD)
tabela_completaD[,3] = tabela_completa[,5]
tabela_completaD[,4] = tabela_dummy

#Area Basal ABr =  dor na tabela
for(dr in 1:qtd_linhas){
  
  area_basal <- round(( tabela_ranqueada_dor$dor/100)*  t_area_basal, digits = 5)
  
}
dim(area_basal) <- c(qtd_linhas,1) # transforma um vetor em matriz

soma_area <- 0
x_area <- matrix(0,qtd_linhas,1) #cria uma matriz de 0's 
for (area in 1:qtd_linhas) {
  teste_area <- sum(area_basal[area,]) #pega o valor de cada linha da matriz 
  soma_area <- round(soma_area + teste_area, digits = 5) #soma o valor das linhas da coluna
  
  if (soma_area <= (t_area_basal/2)) {
    x_area[area,1] <- 1 #escreve variÃƒÂ¡vel dummy na matriz
  }
  
}

index_F = index_dor;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_area
source(caminhoColD)
tabela_completaD[,5] = tabela_completa[,6]
tabela_completaD[,6] = tabela_dummy

#Valor comercial da madeira (vcmr)
for (vc in 1:qtd_linhas) {
  
  valor_comercial_madeira_especie <- round((tabela_ranqueada_vcmr$vcmr/100) * valor_total_comercial_madeira, digits = 5)
}
dim(valor_comercial_madeira_especie) <- c(qtd_linhas,1) #transforma um vetor em matriz

soma_vcmr <- 0
x_vcmr <- matrix(0, qtd_linhas, 1)
for (vcm in 1:qtd_linhas) {
  teste_vcmr <- sum(valor_comercial_madeira_especie[vcm,])
  
  soma_vcmr <- round(soma_vcmr + teste_vcmr, digits = 5)
  
  if (soma_vcmr <= (valor_total_comercial_madeira/2)) {
    x_vcmr[vcm,1] <- 1
  }
}

index_F = index_vcmr;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_vcmr
source(caminhoColD)
tabela_completaD[,7] = tabela_completa[,7]
tabela_completaD[,8] = tabela_dummy

#########primerio corte

#biomassa
for (bio in 1:qtd_linhas) {
  
  biomassa_especie <- round((tabela_ranqueada_biomr$biomr/100) * t_biomassa_especies, digits = 5)
}
dim(biomassa_especie) <- c(qtd_linhas,1)

soma_bio <- 0
x_bio <- matrix(0,qtd_linhas,1)
for(biom in 1:qtd_linhas){
  teste_biom <- sum(biomassa_especie[biom,])
  
  soma_bio = round(soma_bio + teste_biom, digits = 5)
  
  if(soma_bio <= (t_biomassa_especies/2)){
    x_bio[biom,1] <- 1
  }
}
index_F = index_biomr;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_bio

source(caminhoColD)

tabela_completaD[,9] = tabela_completa[,8]
tabela_completaD[,10] = tabela_dummy
#PFNM
for (pf in 1:qtd_linhas) {
  
  pfmn_especie <- round((tabela_ranqueada_usor$usor/100) * t_uso_especie, digits = 5)
}

dim(pfmn_especie) <- c(qtd_linhas,1)

soma_pfnmr <- 0
x_pfnmr <- matrix(0, qtd_linhas, 1)
for (pfnm in 1:qtd_linhas) {
  teste_pfnm <- sum(pfmn_especie[pfnm,1])
  soma_pfnmr= round(soma_pfnmr + teste_pfnm, digits = 5)
  
  if(soma_pfnmr <= (t_uso_especie/2)){
    x_pfnmr[pfnm,1] <- 1
    
  }
}
index_F = index_usor
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_pfnmr

source(caminhoColD)

tabela_completaD[,11] = tabela_completa[,9]
tabela_completaD[,12] = tabela_dummy
#-----------------------------------------------------------Outra parte

##Transforma em dataFrame
tabela_completaD <- as.data.frame.matrix(tabela_completaD)


##Dando nomes as colunas
names(tabela_completaD) <- c("abr", "abr-D", "fqr", "fqr-D", "dor", "dor-D", "vcmr", "vcmr-D", "biomr", "biomr-D", "usor", "usor-D")

tabela_completaD$abr <- as.character(tabela_completaD$abr)
tabela_completaD$abr <- as.numeric(tabela_completaD$abr)

tabela_completaD$fqr <- as.character(tabela_completaD$fqr)
tabela_completaD$fqr <- as.numeric(tabela_completaD$fqr)

tabela_completaD$dor <- as.character(tabela_completaD$dor)
tabela_completaD$dor <- as.numeric(tabela_completaD$dor)

tabela_completaD$vcmr <- as.character(tabela_completaD$vcmr)
tabela_completaD$vcmr <- as.numeric(tabela_completaD$vcmr)

tabela_completaD$biomr <- as.character(tabela_completaD$biomr)
tabela_completaD$biomr <- as.numeric(tabela_completaD$biomr)

tabela_completaD$usor <- as.character(tabela_completaD$usor)
tabela_completaD$usor <- as.numeric(tabela_completaD$usor)

inf1 <- sapply(tabela_completaD, summary)

inf2 <- sapply(tabela_completaD, sd)

inf3 <-  cor(tabela_completaD)

fit <- princomp(tabela_completaD, cor = TRUE)

sumario <- summary(fit)

carregados <- loadings(fit)

# plot(fit, type="line")

# fit$scores


tabelaSco <- as.data.frame(fit$scores)

# biplot(fit)

sumarioTabela <- summary(tabelaSco)

fit <- principal(tabela_completaD, nfactors = 3, rotate = "varimax", scores = TRUE)

fatores <- as.data.frame(fit$scores)

fatoresLinhas <- nrow(fatores)

fatoresPz <- matrix(0, fatoresLinhas,3)

fatoresPz[,1] <- fatores[,1]
fatoresPz[,2] <- fatores[,2]
fatoresPz[,3] <- fatores[,3]

for (p in 1:3) {
  
  for (l in 1:fatoresLinhas) {
    
    fatoresPz[l,p] <- ((fatores[l,p] - min(fatores[,p]))/(max(fatores[,p]) - min(fatores[,p])))
    
  }
}

loadings_fac1 = fit$loadings[,1]
l1 = round(sum(loadings_fac1^2), digits = 5)

loadings_fac1 = fit$loadings[,2]
l2 = round(sum(loadings_fac1^2), digits = 5)

loadings_fac1 = fit$loadings[,3]
l3 = round(sum(loadings_fac1^2), digits = 5)

somaLT <- round(l1 + l2 + l3, digits = 5)

lb1 = round(l1/somaLT, digits = 5)
lb2 = round(l2/somaLT, digits = 5)
lb3 = round(l3/somaLT, digits = 5)

fatoresPVLB <- matrix(0, fatoresLinhas, 3)


fatoresPVLB[, 1] <- round(fatoresPz[, 1] * lb1, digits = 5)
fatoresPVLB[, 2] <- round(fatoresPz[, 2] * lb2, digits = 5)
fatoresPVLB[, 3] <- round(fatoresPz[, 3] * lb3, digits = 5)

ifse <- matrix(0, fatoresLinhas, 1)

for (k in 1:fatoresLinhas) {
  
  ifse[k,] <- round(fatoresPVLB[k,1] + fatoresPVLB[k,2] + fatoresPVLB[k,3], digits=5)
}

ifsePor <- matrix(0, fatoresLinhas, 1)

for (z in 1:fatoresLinhas) {
  
  ifsePor[z,] <- round((ifse[z,]/sum(ifse))*100, digits = 5)
}

ampcat <- ((max(ifsePor)- min(ifsePor))/3)
catlms1 <- max(ifsePor)
catlmi1 <- max(ifsePor) - ampcat

catlms3 <- min(ifsePor) + ampcat
catlmi3 <- min(ifsePor)

catlms2 <- catlmi1 - 0.001
catlmi2 <- catlms3 + 0.001

categorias <- matrix(0, fatoresLinhas, 1)

for (w in 1:fatoresLinhas) {
  
  if(ifsePor[w,1] <= catlms1 && ifsePor[w,1] >= catlmi1  ){
    categorias[w,1] <- 1
  } else if(ifsePor[w,1] <= catlms3 && ifsePor[w,1] >= catlmi3  ){
    categorias[w,1] <- 3
  } else{
    categorias[w,1] <- 2
  }
}

tabela_completa$ifse <- ifelse( tabela_completaD$abr > 1, 1, 0)

tabela_completa$ifse_p <- ifelse( tabela_completaD$abr > 1, 1, 0)

tabela_completa$Cat <- ifelse( tabela_completaD$abr > 1, 1, 0)

tabela_completa[,10] <- ifse
tabela_completa[,11] <- ifsePor
tabela_completa[,12] <- categorias

index_ifse<- with(tabela_completa, order(ifse_p, ifse ,Cat, abr,fqr, dor, vcmr, biomr, usor, decreasing = TRUE))
tabela_completa =  tabela_completa[index_ifse,] 

######Parte do plantio.

# tabela_completa$plantio <- 0
# 
# for (i in 1:qtd_linhas) {
#   
#   calculado = tabela_completa[i,4] * 1.1
#   tabela_completa[i,11] = (1667 * calculado)/ 100
#   
# }
# 
# tabela_completa[1:qtd_linhas, 11] <- as.integer(tabela_completa[1:qtd_linhas, 11])
# 
# for (t in 1:qtd_linhas) {
#   if (tabela_completa[t,11]< 10 && tabela_completa[t,11] != 0) {
#     tabela_completa[t,11] = 10
#     
#   }
# }
# 
# index_plantio <- with(tabela_completa, order(ifse,plantio ,usor, abr, fqr, dor, vcmr, biomr, decreasing = TRUE))
# tabela_completa = tabela_completa[index_plantio,]

#nomeCientifico <- as.data.frame(tabela_completa$nome_cientifico)
#nomePopular <- as.data.frame(tabela_completa$nome_popular)
#indicefse <- as.data.frame(tabela_completa$ifse)
#indiceUsos <- as.data.frame(tabela_completa$usor)
#indicePlantio <- as.data.frame(tabela_completa$plantio)

# print(nomeCientifico, row.names = FALSE)
# print(nomePopular, row.names = FALSE)
# print(indiceUsos, row.names = FALSE)
# print(indicefse, row.names = FALSE, digits = 3)
# print(indicePlantio, row.names = FALSE)


titulos = colnames(tabela_completa)
totenGuard = "["
totenGuardF = "]"
listaTotal = c()
chaves = "{"
chavesI = "}"
virgula = ","

for (a in 1:qtd_linhas) {listarow = c()

for(b in 1:12){
  
  pcoluna = titulos[b]
  pcoluna = paste("\"",pcoluna, "\"", sep = "")
  valorp = tabela_completa[a,b]
  valorp = paste("\"",valorp, "\"", sep = "")
  
  variValor = paste(pcoluna, valorp, sep=":") 
  
  if(b == 1){variValor = paste(chaves, variValor, sep = "") }
  
  listarow = append(listarow, variValor ,b)
  totenGuard = paste( totenGuard,  listarow[b], sep = "")
  
  # variValor = paste(chaves ,variValor, sep = "")
  #  variValor = paste(variValor, chavesI, sep = "")
  
  if(b == 12){totenGuard = paste(totenGuard,chavesI,  sep = "") }
  
  if(b != 12 || a != qtd_linhas){
    totenGuard = paste(totenGuard, virgula, sep ="")
  }
  
}


}
listaTotal = append(listaTotal, totenGuard ,a)
listaTotal = paste(listaTotal, totenGuardF, sep = "")

#listaTotal
cat(listaTotal)
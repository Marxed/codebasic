//marxed
var rootPath = "/BasaWeb/";

//var jogoid;

var basApp = angular.module('basApp', ['ngRoute',]);

basApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
                .when('/Dashboard', {
                    templateUrl: 'views/basApp/Dashboard.html',
                    controller: 'Dashboardreal'
                }).
                when('/Inventario-Cadastro', {
                    templateUrl: 'views/basApp/dashboard_1.html',
                    controller: 'Dashboard'
                }).
                when('/Dashboard-admin', {
                    templateUrl: 'views/Dashboard_admin.html',
                    controller: 'Dashboard_admin'
                }).
                when('/teste', {
                    templateUrl: 'views/Inicio.html'
                }).
                when('/Inventario-Consulta', {
                    templateUrl: 'views/basApp/Inventario_consulta.html',
                 
                    controller: 'Tabela_inventario'
                }).
                when('/Inventario-Selecao', {
                    templateUrl: 'views/basApp/Inventario_selecao.html',
                 
                    controller: 'Inventario_selecao'
                }).
                when('/Perfil', {
                    templateUrl: 'views/basApp/PerfilUsuario.html',
                    controller: 'Perfil'
                    
                }).
                    when('/Logout', {
                    templateUrl: 'views/basApp/PerfilUsuario.html',
                    controller: 'Logout'
                    
                }).
                otherwise({
                    redirectTo: '/Dashboard'
                });
                
    }]);



basApp.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
              //  alert("directive");
                var modelSetter = model.assign;
                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

basApp.service('fileUpload', ['$http', 'returnresponse', function ($http, returnresponse) {
        this.uploadFileToUrl = function (file, uploadUrl, nomeQ, id) {
            var fd = new FormData();
            fd.append('file', file);
            fd.append('nome', nomeQ);
            fd.append('usuarioId', id);
           // alert("service");
//            if(data !== undefined){
//                for(var key in data){
//                    fd.append(key, data[key]);
//                }
//            }
        $("#myModal").removeClass('hide');

        document.getElementById('sendForm').style.visibility="hidden";
//        document.getElementById("progressbar").style.display = 'block';
//        document.getElementById("sendForm").style.display = 'none';
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })

                    .success(function (resp) {
                        alert(resp.resp);
                $("#myModal").addClass('hide');
                document.getElementById('sendForm').style.visibility="visible";
                if(resp.resp == "Inventário salvo com Sucesso"){
                returnresponse.set(0);
                location.href = "/BasaWeb/index.html#/Inventario-Selecao";
                } else {
                 return resp.resp;
                }
                    })

                    .error(function (resp) {
                 $("#myModal").addClass('hide');
                returnresponse.set(1);
                document.getElementById('sendForm').style.visibility="visible";
                return resp.resp;
                alert("Dentro do error");
                    });
        };
    }]);

  basApp.factory('passaInventario', function() {
 var savedData = {}
 function set(data) {
   savedData = data;
 }
 function get() {
  return savedData;
 }

 return {
  set: set,
  get: get
 };

});

  basApp.factory('returnresponse', function() {
 var response = {}
 function set(data) {
   response = data;
 }
 function get() {
  return response;
 }

 return {
  set: set,
  get: get
 };

});


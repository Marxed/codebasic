
import br.ufra.basaweb.dao.EspecieInventarioDao;
import br.ufra.basaweb.dao.InventarioDao;
import br.ufra.basaweb.dao.UsuarioDao;
import br.ufra.basaweb.model.EspecieInventario;
import br.ufra.basaweb.model.EspecieRegiao;
import br.ufra.basaweb.model.Especies;
import br.ufra.basaweb.model.Inventario;
import br.ufra.basaweb.model.Usuario;
import br.ufra.basaweb.util.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Beatriz Nery
 */
public class MainTeste {
    public static void main(String[] args){
        Session oSession = HibernateUtil.getSession();
        
        Usuario user = UsuarioDao.obterUsuarioId(1);
        
        List<Inventario> inventarios = InventarioDao.obterPorUsuario(user);
        
        Inventario inventario = inventarios.get(0);
        System.out.println("############################");
        System.out.println(inventario.getInventarioId());
        
        List<EspecieInventario> espinvs = EspecieInventarioDao.obterPorInventario(inventario);
        
        for(EspecieInventario e : espinvs){
            System.out.println(e.getId().getEspecieInventarioId());
            EspecieRegiao er = e.getEspecieRegiao();
            System.out.println("Nome Cientifico\tNome Popular\tFamilia\tNº de Individuos\tÁrea Basal m²\tVolume m³\tFrequencia\tValor Comercial da Madeira (VCM)\tBiomassa</th>\tUtilidade da Espécie<");
            Especies esp = e.getEspecieRegiao().getEspecies();
            System.out.print(esp.getNomeCientifico()+"\t");
            System.out.print(e.getEspecieRegiao().getNomePopular()+"\t");
            System.out.print(e.getEspecieRegiao().getEspecies().getFamilia()+"\t");
            System.out.print(e.getNumeroIndividuos()+"\t");
            System.out.print(e.getAreaBasal()+"\t");
            System.out.print(e.getVolume()+"\t");
            System.out.print(e.getFrequencia()+"\t");
            System.out.print(e.getEspecieRegiao().getValorComercial()+"\t");
            System.out.print(e.getBiomassa()+"\t");
            System.out.print(e.getEspecieRegiao().getEspecies().getNumeroUsos()+"\t");
        }
        System.out.println("############################");
    }
}

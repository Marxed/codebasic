var basaControllers = angular.module('basApp');

basaControllers.controller('Perfil', ['$scope', '$http',
    function ($scope, $http) {

        $scope.regInfo = true;

        $scope.clickBtn = function ()
        {
            $scope.regInfo = true;
        };

        $scope.init = function(){
            $scope.usuario = JSON.parse(sessionStorage.usuario);
            var n = JSON.parse(sessionStorage.nome);
            $scope.usuario.nome = n;
        };
        $scope.init();
        
        try
        {
            $scope.atualizar = function ()
            {
                var userPassword = $scope.usuario.oldPassword;
                var newUserPassword = $scope.usuario.newPassword;
                var newPasswordCfrm = $scope.usuario.newPasswordCfrm;

                if (userPassword !== undefined && newUserPassword !== undefined)
                {
                    $scope.regInfo = true;

                    if (newUserPassword !== newPasswordCfrm) {
                        $scope.errMsg = 'Senhas diferentes! Nova Senha e Confirm. Nova Senha devem ser iguais.';
                        $scope.regInfo = false;
                        return;
                    }

                    var fd = new FormData();
                    fd.append("usuarioId", parseInt($scope.usuario.idUsuario));
                    fd.append("senha", userPassword);
                    fd.append("novaSenha", newUserPassword);
                    
                    $http.post(rootPath + "Usuario/update", fd, {headers: {'Content-Type': undefined}})
                            .success(function (resp)
                            {
                                
                                if (resp.code) {
                                    $scope.errMsg = resp.msg;
                                    $scope.regInfo = false;
                                    return;

                                }
                                console.log(resp);
                                var usuario = {};
                                usuario.nome = $scope.usuario.nome;
                                usuario.idUsuario = $scope.usuario.idUsuario;
                                usuario.email = $scope.usuario.email;
                                sessionStorage.usuario = JSON.stringify(usuario);
                                $scope.Msg = "Usuario alterado com sucesso.";
                            })
                            .error(function (response)
                            {
                                $scope.errMsg = "Falha ao conectar com o servidor, verifique sua conxão com a internet. Caso sua internet estaja ok, os administradores do sistema já foram notificados do problema ocorrido.";
                                $scope.autInfo = false;
                                throw JSON.stringify(response);
                            });

                }
                else if (userPassword === undefined)
                {
                    $scope.errMsg = 'O campo "Senha Atual" deve ser preenchido.';
                    $scope.regInfo = false;
                }
                else if (newUserPassword === undefined){
                    $scope.errMsg = 'O campo "Nova Senha" deve ser preenchido.';
                    $scope.regInfo = false;
                }
                else if (newPasswordCfrm === undefined){
                    $scope.errMsg = 'O campo "Confirmar Nova Senha" deve ser preenchido.';
                    $scope.regInfo = false;
                }
            };
        }
        catch (e)
        {
            var fd = new FormData();
            var timestamp = new Date().getTime();
            var msg = "Erro em rotina de cadastro de usuário";
            var stacktrace = e.stack;

            fd.append("timestamp", timestamp);
            fd.append("msg", msg);
            fd.append("stacktrace", stacktrace);

            
        }
    }]);


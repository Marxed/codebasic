var basaControllers = angular.module('basApp');

basaControllers.controller('MenuController', ['$scope', '$location',
    function ($scope,  $location) {
        
        $scope.usuario = {};
        
        $scope.init = function(){
            if(sessionStorage.usuario){
                $scope.usuario = JSON.parse(sessionStorage.usuario);
            } else {
                location.href = "Home.html";
            }
            
        };
        $scope.init();
        
        $scope.isActive = function (viewLocation) {
            return viewLocation === $location.path();
        };
        
        $scope.logoff = function(){
            sessionStorage.removeItem("usuario");
            location.href = "Home.html";
        };

    }]);
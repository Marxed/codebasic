var rootPath = "/BasaWeb/";

var autentica = angular.module('autentica', [
  'ngRoute'
]);


autentica.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
            .when('/Cadastro', {
        templateUrl: 'views/autentica/page-cadastro.html',
        controller: 'Cadastro'
      }).
      when('/Login', {
        templateUrl: 'views/autentica/pages-login.html',
        controller: 'Login'
      }).
      when('/Senha', {
        templateUrl: 'views/autentica/RememberPass.html',
        controller: 'Senha'
      }).       
      otherwise({
        redirectTo: ''
      });
  }]);

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.dao;

import br.ufra.basaweb.model.EspecieInventarioId;
import br.ufra.basaweb.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Beatriz Nery
 */
public class EspecieInventarioIdDao {
      
    public static EspecieInventarioId obterPorId(int especieInventarioId, int especieRegiaoEspecieRegiaoId,  int inventarioInventarioId ) {
          try{
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(EspecieInventarioId.class);
        criteria.add(Restrictions.eq("EspecieInventarioId", new EspecieInventarioId(especieInventarioId, especieRegiaoEspecieRegiaoId, inventarioInventarioId)));
        List<EspecieInventarioId> especieInventarioIds = criteria.list();
//        session.close();
        if (especieInventarioIds.isEmpty()) {
            return null;
        }
        return especieInventarioIds.get(0);
          }catch(Exception ex){
              ex.printStackTrace();
              return null;
          }
    }
    
}

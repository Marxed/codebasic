/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.dao;

import br.ufra.basaweb.model.EspecieInventario;
import br.ufra.basaweb.model.EspecieInventarioId;
import br.ufra.basaweb.model.Inventario;
import br.ufra.basaweb.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Beatriz Nery
 */
public class EspecieInventarioDao {
    
    
    
    public static List<EspecieInventario> listar(EspecieInventario especieInventario){
        
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(EspecieInventario.class);
        List<EspecieInventario> especieInventarios = criteria.list();
        return especieInventarios;
        
    }
    
     public static List<EspecieInventario> obterPorInventario(Inventario inventario) {
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(EspecieInventario.class);
        criteria.add(Restrictions.eq("inventario", inventario));
        List<EspecieInventario> especieInventarios = criteria.list();
        //session.close();
        if (especieInventarios.isEmpty()) {
            return null;
        }
        return especieInventarios;
    }
     
     
     
     public static boolean update(EspecieInventario especieInventario) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.update(especieInventario);
            transaction.commit();
//            session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
     
      public static boolean insert(EspecieInventario especieInventario) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.save(especieInventario);
            transaction.commit();
            session.close();
//            session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
       
     //Classe EspecieInvemtario nao tem id // Tem mas e objeto
     
    public static EspecieInventario obterPorId(EspecieInventarioId especieInventarioId) {
          try{
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(EspecieInventario.class);
        criteria.add(Restrictions.eq("id", especieInventarioId));
        List<EspecieInventario> especieInventario = criteria.list();
        //session.close();
        if (especieInventario.isEmpty()) {
            return null;
        }
        return especieInventario.get(0);
          }catch(Exception ex){
              ex.printStackTrace();
              return null;
          }
    }
    
     
    
     public static boolean delete(EspecieInventario especieInventario) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.delete(especieInventario);
            transaction.commit();
//            session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
     public static boolean delete(List<EspecieInventario> especieInventarios) {
        try {
           
            
           for(EspecieInventario e : especieInventarios)   {
                Session session = HibernateUtil.getSession();
               Transaction transaction = session.beginTransaction();
                session.delete(e);
                transaction.commit();
                session.close();
        }
//        Query q = session.createQuery("delete Entity where id = X");
//        q.executeUpdate();
            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
    
    
}

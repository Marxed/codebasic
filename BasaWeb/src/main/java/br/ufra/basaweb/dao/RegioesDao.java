/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.dao;

import br.ufra.basaweb.model.Especies;
import br.ufra.basaweb.model.Regioes;
import br.ufra.basaweb.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Souza-pc
 */
public class RegioesDao {
    
    public static Regioes obterPorId(int id) {
          try{
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(Regioes.class);
        criteria.add(Restrictions.eq("regiaoId", id));
        List<Regioes> regioes = criteria.list();
        //session.close();
        if (regioes.isEmpty()) {
            Regioes r = new Regioes();
            r.setRegiaoId(1);
            r.setRegiao("norte");
            session.flush();
            session.save(r);
            session.close();
            return r;
        }
        return regioes.get(0);
          }catch(Exception ex){
              ex.printStackTrace();
              return null;
          }
    }
    
}

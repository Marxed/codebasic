/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.dao;

import br.ufra.basaweb.model.Inventario;
import br.ufra.basaweb.model.Usuario;
import br.ufra.basaweb.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Beatriz Nery
 */
public class InventarioDao {

    public static boolean upload() {

        return false;
    }
    
    public static Inventario obterPorId(int id) {
          try{
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(Inventario.class);
        criteria.add(Restrictions.eq("inventarioId", id));
        List<Inventario> inventarios = criteria.list();
        session.close();
        if (inventarios.isEmpty()) {
            return null;
        }
        return inventarios.get(0);
          }catch(Exception ex){
              ex.printStackTrace();
              return null;
          }
    
    
    }
    
    public static List<Inventario> obterPorUsuario(Usuario user) {
          try{
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(Inventario.class);
        criteria.add(Restrictions.eq("usuario", user));
        List<Inventario> inventarios = criteria.list();
        //session.close();
        return inventarios;
        
          }catch(Exception ex){
              ex.printStackTrace();
              return null;
          }
    
    
    }
    public static boolean insert(Inventario Inventario) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.save(Inventario);
            transaction.commit();
            session.close();
//            session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
    
     public static boolean delete(Inventario inventario) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.delete(inventario);
            transaction.commit();
          session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
   

     
}

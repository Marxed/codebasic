/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.dao;

import br.ufra.basaweb.model.EspecieRegiao;
import br.ufra.basaweb.model.Especies;
import br.ufra.basaweb.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Beatriz Nery
 */
public class EspecieDao {
    
     public static List<Especies> listar(Especies especie) {
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(Especies.class);
        List<Especies> especies = criteria.list();
        return especies;
    }
     
     public static boolean delete(Especies especies) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            List<EspecieRegiao> especieRegiao = EspecieRegiaoDao.listarPorEspecie(especies);
            for(EspecieRegiao e: especieRegiao){
                
                EspecieRegiaoDao.delete(e);
                
            }
            session.delete(especies);
            transaction.commit();
//            session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
     
      public static Especies obterPorId(int id) {
          try{
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(Especies.class);
        criteria.add(Restrictions.eq("especieId", id));
        List<Especies> especies = criteria.list();
        session.close();
        if (especies.isEmpty()) {
            return null;
        }
        return especies.get(0);
          }catch(Exception ex){
              ex.printStackTrace();
              return null;
          }
    }
      public static Especies obterPorNomeCientifico(String nome) {
          try{
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(Especies.class);
        criteria.add(Restrictions.eq("nomeCientifico", nome));
        List<Especies> especies = criteria.list();
        session.close();
        if (especies.isEmpty()) {
            return null;
        }
        return especies.get(0);
          }catch(Exception ex){
              ex.printStackTrace();
              return null;
          }
    }
      
      public static boolean update(Especies especies) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.update(especies);
            transaction.commit();
//            session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
      public static boolean insert(Especies especies) {
        try {
            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            session.save(especies);
            transaction.commit();
            session.close();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }
}

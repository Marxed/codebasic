/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.util;

import com.sun.net.ssl.internal.ssl.Provider;
import java.io.File;
import java.security.Security;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 *
 * @author Beatriz
 */
public class Email {
    
    private static String SMTP_HOST_NAME = "smtp.gmail.com";

    private static String SMTP_AUTH_USER = "projetoufrabasa@gmail.com";

    private static String SMTP_AUTH_PWD = "projetobasa";
    private String para;
    private String mensagem;
    private File[] files;
    private DateFormat formatter = new SimpleDateFormat(
            "dd/MM/yyyy HH:mm:ss.SSS");
    
    public Email(String data){
        this.para = "projetoufrabasa@gmail.com";
        SMTP_HOST_NAME = "smtp.gmail.com";
        SMTP_AUTH_USER = "projetoufrabasa@gmail.com";
        SMTP_AUTH_PWD = "projetobasa";
        this.mensagem = data;
        
        try{
            postMail(this.para, "BASA - Cadastro", this.mensagem, SMTP_AUTH_USER);
        }catch(MessagingException ex){
            System.err.println("Falha ao enviar email para " + this.para + ": " + ex.getMessage());
        }
        
    }
    
    public Email(String data, File[] files){
        this.para = "projetoufrabasa@gmail.com";
        SMTP_HOST_NAME = "smtp.gmail.com";
        SMTP_AUTH_USER = "projetoufrabasa@gmail.com";
        SMTP_AUTH_PWD = "projetobasa";
        this.mensagem = data;
        this.files = files;
        
        try{
            postMailAnexo(this.para, "BASA - ERRO", this.mensagem, this.files, SMTP_AUTH_USER);
        }catch(MessagingException ex){
            System.err.println("Falha ao enviar email para " + this.para + ": " + ex.getMessage());
        }
        
        
    }
    
    public void postMail(String recipients, String subject, String message, String from) throws MessagingException{
        boolean debug = false;

        Security.addProvider(new Provider());

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.transport.protocol", "smtp");

        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator();
        Session session = Session.getInstance(props, auth);

        session.setDebug(debug);

        Message msg = new MimeMessage(session);

        InternetAddress addressFrom = new InternetAddress(from);
        msg.setFrom(addressFrom);

        msg.setSubject(subject);
        msg.setContent(message, "text/plain");

        InternetAddress addressTo;
        addressTo = new InternetAddress(recipients);
        msg.setRecipient(Message.RecipientType.TO, addressTo);
        try {
            Transport.send(msg);
            System.out.println(formatter.format(new Date()) + " [smqee.extracao.Extrair] => Email enviado: " + recipients);
        } catch (Exception e) {
            System.out.println(formatter.format(new Date()) + " [smqee.extracao.Extrair] => Falha ao enviar email: " + recipients);
            if (!e.getMessage().equals("Invalid Addresses;")) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    public void postMailAnexo(String recipients, String subject, String message, File[] files, String from) throws MessagingException {
        boolean debug = false;

        Security.addProvider(new Provider());

        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.transport.protocol", "smtp");

        props.put("mail.smtp.host", SMTP_HOST_NAME);
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");

        Authenticator auth = new SMTPAuthenticator();
        Session session = Session.getInstance(props, auth);

        session.setDebug(debug);

        Message msg = new MimeMessage(session);
        MimeBodyPart txt = new MimeBodyPart();

        InternetAddress addressFrom = new InternetAddress(from);
        msg.setFrom(addressFrom);

        msg.setSubject(subject);
        msg.setContent(message, "text/plain");
        txt.setText(message);
        Multipart mp = new MimeMultipart();
        for (File f : files) {
            DataSource fds = new FileDataSource(f);
            MimeBodyPart mbp = new MimeBodyPart();
            mbp.setDisposition(Part.ATTACHMENT);
            mbp.setDataHandler(new DataHandler(fds));
            mbp.setFileName(fds.getName());
            mp.addBodyPart(mbp);
        }
        mp.addBodyPart(txt);
        msg.setContent(mp);

        InternetAddress addressTo;
        addressTo = new InternetAddress(recipients);
        msg.setRecipient(Message.RecipientType.TO, addressTo);
        try {
            Transport.send(msg);
            System.out.println(formatter.format(new Date()) + " [smqee.extracao.Extrair] => Email enviado: " + recipients);
        } catch (Exception e) {
            System.out.println(formatter.format(new Date()) + " [smqee.extracao.Extrair] => Falha ao enviar email: " + recipients);
            if (!e.getMessage().equals("Invalid Addresses;")) {
                System.out.println(e.getMessage());
            }
        }
    }
    
    
    private class SMTPAuthenticator extends Authenticator{
        private SMTPAuthenticator(){
            
        }
        
        public PasswordAuthentication getPasswordAuthenticator(){
            String username = Email.SMTP_AUTH_USER;
            String password = Email.SMTP_AUTH_PWD;
            return new PasswordAuthentication(username, password);
        }
    }
    
}

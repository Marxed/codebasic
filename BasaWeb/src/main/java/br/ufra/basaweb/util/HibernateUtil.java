/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Beatriz Nery
 */
public class HibernateUtil {

    private static SessionFactory factory;
    
    static Session session = null;
    
    static {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            factory = null;
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() {
        if(session == null){
            session = factory.openSession();
        }
        if(!session.isOpen()){
            session = factory.openSession();
        }
        return session;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.util;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Beatriz Nery
 */
public class JavaMail {
    
    static Properties mailServerProperties;
    private static Session getMailSession;
    private static MimeMessage generateMailMessage;
    
    private static String para;
    
    
    public static void main(String args[]) throws AddressException, MessagingException{
        
        generateAndSendEmail();
        
        System.out.println("\n\n ===> Send Email.");
        
    }
    
    public static void generateAndSendEmail() throws AddressException, MessagingException{
        System.out.println("\n 1st ==> Setup Mail Server properties.");
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        mailServerProperties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        mailServerProperties.put("mail.transport.protocol", "smtp");
        mailServerProperties.put("mail.smtp.connectiontimeout", "60000");
        mailServerProperties.put("mail.smtp.timeout", "60000");
        System.out.println("Mail Server Properties have been setup succesfully");
        
        
        System.out.println("Get mail Session");
        setGetMailSession(Session.getDefaultInstance(mailServerProperties, null));
        setGenerateMailMessage(new MimeMessage(getGetMailSession()));
        getGenerateMailMessage().addRecipient(Message.RecipientType.TO, new InternetAddress("projetoufrabasa@gmail.com"));
        getGenerateMailMessage().addRecipient(Message.RecipientType.CC, new InternetAddress("beatriznery.12@gmail.com"));
        getGenerateMailMessage().setSubject("Sistema Restaura");
        String emailBody = "Voce acabou de se Registrar.<br>Login";
        getGenerateMailMessage().setContent(emailBody, "text/html");
        System.out.println("Mail Session Succesfully..");
        
        
        System.out.println("Get Session and Send Mail");
        Transport transport =  getGetMailSession().getTransport("smtp");
        
        
        transport.connect("smtp.gmail.com", 587, "projetoufrabasa@gmail.com", "projetobasa");
        transport.sendMessage(getGenerateMailMessage(), getGenerateMailMessage().getAllRecipients());
        transport.close();
        System.out.println("Email enviado");
    }
    
    public static void sendEmailSuccess() throws AddressException, MessagingException{
        mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.port", "587");
        mailServerProperties.put("mail.smtp.auth", "true");
        mailServerProperties.put("mail.smtp.starttls.enable", "true");
        System.out.println("Mail Server Properties have been setup succesfully");
        
        setGetMailSession(Session.getDefaultInstance(mailServerProperties, null));
        setGenerateMailMessage(new MimeMessage(getGetMailSession()));
        getGenerateMailMessage().addRecipient(Message.RecipientType.TO, new InternetAddress("projetoufrabasa@gmail.com"));
        getGenerateMailMessage().addRecipient(Message.RecipientType.CC, new InternetAddress(getPara()));
        getGenerateMailMessage().setSubject("Sistema Restaura");
        String emailBody = "Voce acabou de se Registrar.<br>Login";
        getGenerateMailMessage().setContent(emailBody, "text/html");
        
        Transport transport =  getGetMailSession().getTransport("smtp");
        
        
        transport.connect("smtp.gmail.com", "projetoufrabasa@gmail.com", "projetobasa");
        transport.sendMessage(getGenerateMailMessage(), getGenerateMailMessage().getAllRecipients());
        transport.close();
    }

    

    /**
     * @return the getMailSession
     */
    public static Session getGetMailSession() {
        return getMailSession;
    }

    /**
     * @param aGetMailSession the getMailSession to set
     */
    public static void setGetMailSession(Session aGetMailSession) {
        getMailSession = aGetMailSession;
    }

    /**
     * @return the generateMailMessage
     */
    public static MimeMessage getGenerateMailMessage() {
        return generateMailMessage;
    }

    /**
     * @param aGenerateMailMessage the generateMailMessage to set
     */
    public static void setGenerateMailMessage(MimeMessage aGenerateMailMessage) {
        generateMailMessage = aGenerateMailMessage;
    }

    /**
     * @return the para
     */
    public static String getPara() {
        return para;
    }

    /**
     * @param aPara the para to set
     */
    public static void setPara(String aPara) {
        para = aPara;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.util;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Beatriz
 */
public class EmailServive {
    
    private static final Logger logger = LoggerFactory.getLogger(EmailServive.class);
    
    private static String userName = "projetoufrabasa@gmail.com";
    private static String password = "projetobasa";
    
    public String sendEmail(String toEmailAddress, String emailSubject, String emailMessage){
        try{
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.connectiontimeout", "60000");
            props.put("mail.smtp.timeout", "60000");
            
            Authenticator auth = new SMTPAuthenticator();
            Session session = Session.getInstance(props, auth);
            
            //email message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(getUserName()));
            String[] recipientList = toEmailAddress.split(",");
            InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
            int counter = 0;
            for(String recipient : recipientList){
                recipientAddress[counter] = new InternetAddress(recipient.trim());
                counter ++;
                
            }
            message.setRecipients(Message.RecipientType.TO, recipientAddress);
            message.setSubject(emailSubject);
            message.setContent(emailMessage, "text/html");
            
            //send smtp message
            Transport tr = session.getTransport("smtp");
            tr.connect("smtp.gmail.com", 587, getUserName(), getPassword());
            message.saveChanges();
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();
            
            return "Mail sent successfully";
            
            
        }catch(MessagingException e){
            return "Error in method sendEmailNotification" + e;
        }
        
    }
    
    public String sendEmailContato(String toEmailAddress, String emailSubject, String emailMessage){
        try{
            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");
            props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.connectiontimeout", "60000");
            props.put("mail.smtp.timeout", "60000");
            
            Authenticator auth = new SMTPAuthenticator();
            Session session = Session.getInstance(props, auth);
            
            //email message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(getUserName()));
            String[] recipientList = toEmailAddress.split(",");
            InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
            int counter = 0;
            for(String recipient : recipientList){
                recipientAddress[counter] = new InternetAddress(recipient.trim());
                counter ++;
                
            }
            message.setRecipients(Message.RecipientType.TO, recipientAddress);
            message.setRecipients(Message.RecipientType.CC, "salomao@museu-goeldi.br");
            message.setSubject(emailSubject);
            message.setContent(emailMessage, "text/html");
            
            //send smtp message
            Transport tr = session.getTransport("smtp");
            tr.connect("smtp.gmail.com", 587, getUserName(), getPassword());
            message.saveChanges();
            tr.sendMessage(message, message.getAllRecipients());
            tr.close();
            
            return "Mail sent successfully";
            
            
        }catch(MessagingException e){
            return "Error in method sendEmailNotification" + e;
        }
        
    }

    /**
     * @return the userName
     */
    public static String getUserName() {
        return userName;
    }

    /**
     * @param aUserName the userName to set
     */
    public static void setUserName(String aUserName) {
        userName = aUserName;
    }

    /**
     * @return the password
     */
    public static String getPassword() {
        return password;
    }

    /**
     * @param aPassword the password to set
     */
    public static void setPassword(String aPassword) {
        password = aPassword;
    }
    
    
    private class SMTPAuthenticator extends Authenticator{

    private SMTPAuthenticator() {
        
    }
    
    public PasswordAuthentication getPasswordAuthentication(){
        String userName = EmailServive.getUserName();
        String password = EmailServive.getPassword();
        
        return new PasswordAuthentication(userName, password);
  
    
}
    }

}

 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.model;

/**
 *
 * @author marxe
 */
public class TabelaBea {
    private int especieRegiaoId;
    private int inventarioId;
    private int especieInventarioId;
    private int especieId;
    private String nomeCientifico;
    private String nomePopular;
    private String familia;
    private int nIndividuos;
    private double area;
    private double volume;
    private double frequencia;
    private double valorComercial;
    private double biomassa;
    private int usos;
    
    public TabelaBea(EspecieInventario e){
        this.especieInventarioId = e.getId().getEspecieInventarioId();
        this.especieId = e.getEspecieRegiao().getEspecies().getEspecieId();
        this.especieRegiaoId = e.getEspecieRegiao().getEspecieRegiaoId();
        this.inventarioId = e.getInventario().getInventarioId();
        this.nomeCientifico = e.getEspecieRegiao().getEspecies().getNomeCientifico();
        this.nomePopular = e.getEspecieRegiao().getNomePopular();
        this.familia = e.getEspecieRegiao().getEspecies().getFamilia();
        this.nIndividuos = e.getNumeroIndividuos();
        this.area = e.getAreaBasal();
        this.volume = e.getVolume();
        this.frequencia = e.getFrequencia();
        this.valorComercial = e.getEspecieRegiao().getValorComercial();
        this.biomassa = e.getBiomassa();
        this.usos = e.getEspecieRegiao().getEspecies().getNumeroUsos();
    }

    public String getNomeCientifico() {
        return nomeCientifico;
    }

    public String getNomePopular() {
        return nomePopular;
    }

    public String getFamilia() {
        return familia;
    }

    public int getnIndividuos() {
        return nIndividuos;
    }

    public double getArea() {
        return area;
    }

    public double getVolume() {
        return volume;
    }

    public double getFrequencia() {
        return frequencia;
    }

    public double getValorComercial() {
        return valorComercial;
    }

    public double getBiomassa() {
        return biomassa;
    }

    public int getUsos() {
        return usos;
    }

    public int getEspecieRegiaoId() {
        return especieRegiaoId;
    }

    public int getInventarioId() {
        return inventarioId;
    }

    public int getEspecieInventarioId() {
        return especieInventarioId;
    }

    public int getEspecieId() {
        return especieId;
    }
    
    
    
}

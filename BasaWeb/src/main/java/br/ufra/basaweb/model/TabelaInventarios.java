/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author marxe
 */
public class TabelaInventarios {
    private int inventarioId;
    private Date data;
    private int especies;
    private int count;

    
    public TabelaInventarios(Inventario e,int espec,int c){
        this.inventarioId = e.getInventarioId();
        this.data = e.getData();
        this.especies = espec;
        this.count = c;

    }
    
//private String format(Date data){
//    DateFormat outputFormatter = new SimpleDateFormat("dd/MM/yyyy");
//    String resposta = outputFormatter.format(data);
//    return resposta;
//}

    public int getInventarioId() {
        return inventarioId;
    }

    public Date getData() {
        return data;
    }

    public int getEspecies() {
        return especies;
    }

    public int getCount() {
        return count;
    }
    
    
  
    
}

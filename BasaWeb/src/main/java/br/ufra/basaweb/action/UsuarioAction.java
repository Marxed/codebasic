/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.action;

import br.ufra.basaweb.dao.UsuarioDao;
import br.ufra.basaweb.model.NivelAcesso;
import br.ufra.basaweb.model.Usuario;
import br.ufra.basaweb.util.EmailServive;
import com.opensymphony.xwork2.ActionSupport;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;


/**
 *
 * @author Beatriz Nery
 */
@Namespace("/Usuario")
@Result(name = "success", type = "json")
@ParentPackage("json-default")
public class UsuarioAction extends ActionSupport {

    private static final long serialVersionUID = 1L;
    private int usuarioId;
    private String nome;
    private String email;
    private String senha;
    private NivelAcesso nivelAcesso;
    
    private HashMap<String, String> data;
    private int code;
    private String msg;

    private Usuario usuario;
    private String novaSenha;

    @Action("salvar")
    public String salvar() {
        data = new HashMap<>();

        try {
            this.usuario = UsuarioDao.obterPorEmail(email);
            if (usuario != null) {
                code = 12;
                msg = "Email ja cadastrado";
            }else{
                this.usuario = UsuarioDao.obterPorNome(nome);
                if(usuario != null){
                    code = 13;
                    msg = "Nome de usuario ja cadastrado";
                }else{
                    code = 0;
                    String senhaCrip;
                    MessageDigest m  = MessageDigest.getInstance("MD5");
                    m.update(this.senha.getBytes(), 0, this.senha.length());
                    senhaCrip = new BigInteger(1, m.digest()).toString(16);
                    usuario = new Usuario(this.usuarioId, this.nivelAcesso, this.nome, this.email, senhaCrip);
                    usuario.setNivelAcesso(NivelAcessoDao.obterPorId(1));

                    UsuarioDao.salvar(usuario);
                    data.put("idUsuario", "" + usuario.getUsuarioId());
                    msg = "Sucesso";
                    
                    String men = "Olá " + usuario.getNome() + "," + "seu cadastro foi realizado com sucesso."
                            + "\n\nLogin: " +  usuario.getLogin();
                    EmailServive email = new EmailServive();
                    email.sendEmail(usuario.getLogin(), "Restaura - Cadastro", men);
                    
                    System.out.println("Usuário Cadastrado com Sucesso");
                }
            }
        } catch (Exception e) {
            String msg = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()) + "\n"
                    + "Erro ao executar metodo salvar() na Classe Usuario\n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", "\n");
            
            EmailServive mail = new EmailServive();
            mail.sendEmail("projetoufrabasa@gmail.com", "Restaura - Erro", msg);
            this.msg = "Houve um problema durante a execucao da rotina";
            System.err.println("DEU MERDA");
            
            e.printStackTrace(); //melhorar o tratamento.
        }

        return SUCCESS;
    }

    @Action("delete")
    public String delete() {

        return SUCCESS;
    }

    @Action("consultar")
    public String consultar() {

        return SUCCESS;
    }

    @Action("update")
    public String update() {
        System.out.println("Senha"+senha);
        System.out.println("Senha"+novaSenha);
        try {
            
            Usuario usr = UsuarioDao.obterUsuarioId(usuarioId);
            System.out.println(usr.getUsuarioId());
            
            this.usuario = UsuarioDao.obterPorEmail(email);
            if(this.usuario != null && this.usuario.getUsuarioId() != usr.getUsuarioId()){
                
                msg = "Email já cadastrado";
            }else{
                String senhaCrip;
                MessageDigest m = MessageDigest.getInstance("MD5");
                m.update(this.senha.getBytes(), 0, this.senha.length());
                senhaCrip = new BigInteger(1, m.digest()).toString(16);
                if(!senhaCrip.equals(usr.getSenha())){
                    msg = "Senha inválida";
                    return SUCCESS;
                }
                if(!novaSenha.equals("") && novaSenha != null && !novaSenha.equals("undefined")){
                    String senhaCrip2;
                    MessageDigest m2 = MessageDigest.getInstance("MD5");
                    m.update(this.novaSenha.getBytes(), 0, this.novaSenha.length());
                    senhaCrip2 = new BigInteger(1, m.digest()).toString(16);
                    usr.setSenha(senhaCrip2);
                }
                System.out.println(usr.getLogin());
                System.out.println(usr.getNome());
                UsuarioDao.update(usr);
                msg = "Sucesso";

                EmailServive mail = new EmailServive();

                String men = "Olá " + usr.getNome() + "," + " você fez alterações no seu perfil de usuário do Sistema Restaura.";

                mail.sendEmail(usr.getLogin(), "Restaura - Alteração do Perfil do Usuário", men);

                    
                }
            

            
        } catch (Exception e) {
            String msg = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()) + "\n"
                    + "Erro ao executar metodo registrar() na Class Autenticacao\n"
                    + Arrays.toString(e.getStackTrace()).replaceAll(",", "\n");
            EmailServive mail = new EmailServive();
            mail.sendEmail("projetoufrabasa@gmail.com", "Restaura - Erro - Update", msg);
            code = 999;
            this.msg = "Houve um problema durante a execução da rotina. Um notificação ja foi enviada para os administradores do sistema";

        }

        return SUCCESS;
    }

    @Action("listar")
    public String listar() {

        return SUCCESS;
    }

    @Action("logar")
    public String logar() {
       

        return SUCCESS;
    }

    /**
     * @return the usuarioId
     */
    public int getUsuarioId() {
        return usuarioId;
    }

    /**
     * @param usuarioId the usuarioId to set
     */
    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    /**
     * @param nivelAcesso the nivelAcesso to set
     */
    public void setNivelAcesso(NivelAcesso nivelAcesso) {
        this.nivelAcesso = nivelAcesso;
    }

    public HashMap<String, String> getData() {
        return data;
    }

    public void setData(HashMap<String, String> data) {
        this.data = data;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }
    
    

}

package br.ufra.basaweb.action;

import br.ufra.basaweb.dao.EspecieDao;
import br.ufra.basaweb.dao.EspecieInventarioDao;
import br.ufra.basaweb.dao.EspecieRegiaoDao;
import br.ufra.basaweb.dao.InventarioDao;
import br.ufra.basaweb.dao.RegioesDao;
import br.ufra.basaweb.dao.UsuarioDao;
import br.ufra.basaweb.model.EspecieInventario;
import br.ufra.basaweb.model.EspecieInventarioId;
import br.ufra.basaweb.model.EspecieRegiao;
import br.ufra.basaweb.model.Especies;
import br.ufra.basaweb.model.Inventario;
import br.ufra.basaweb.model.Regioes;
import br.ufra.basaweb.model.Usuario;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.enums.CSVReaderNullFieldIndicator;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
//import net.sf.jmimemagic.Magic;
//import net.sf.jmimemagic.MagicMatch;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Marcus
 */
@Namespace("/UploadFileController")
@Result(name = "success", type = "json")
@ParentPackage("json-default")
public class UploadAction extends ActionSupport {

    private String jsonData;
 
    private final String rootPath = ServletActionContext.getServletContext().getRealPath("/");    
    String ext  = ".csv";
    

    private File file;
    private String nome;
    private EspecieInventario especieInventario;
    private EspecieInventarioId especieInventarioid;
    private Inventario inventario;
    private Especies especies;
    private EspecieRegiao especieRegiao;
    private Regioes regioes;
    private Usuario user;
    private String resp;
    private int usuarioId;
    boolean flag;
    java.util.Date dt;
       
     String nomeArq = nome+ext;

    
    
    @Action("upload")
    public String upload() {
     System.out.println(nome);
     /*
     try{
         System.out.println("up1");
        Magic parser = new Magic();
        File filex = new File(file.getAbsolutePath());
        byte[] array = new byte[(int) filex.length()];
               FileInputStream fileInputStream = new FileInputStream(filex);
               fileInputStream.read(array);
               for (int i = 0; i < array.length; i++) {
                           System.out.print((char)array[i]);
                } 
          System.out.println("up4");
          MagicMatch match = new MagicMatch();
          parser.initialize();
          System.out.println("3");
          match = parser.getMagicMatch(array);

          System.out.println("up5");
          System.out.println(match.getMimeType());  
          System.out.println("up6");

          //if (match.getMimeType().equals()){
            //throw new  Exception("Formato Inválido");
          //}
     } catch(Exception e){
         e.printStackTrace();
         //resp = "Formato Inválido";
        // file = null;
     }
   */
        try {
            if (file != null) {
                //File a = new File( rootPath + "filesTest" + File.separator + "" + getNome());
                  
                // getMagicMatch accepts Files or byte[], 
                // which is nice if you want to test streams
                                //a.mkdirs();
              
                //Files.copy(getFile().toPath(), a.toPath(), REPLACE_EXISTING);
                InputStream fileStream = new FileInputStream(file);
                lercsv(fileStream);
            } else {
                System.out.println("File == null");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            resp = "Erro ao salvar Inventário"; 
            return ERROR;
        }
        
        
         
        return SUCCESS;
    }
    
    
    public String lercsv(InputStream fileStream) {
        
        try {
            //CSVReader reader = new CSVReader(new InputStreamReader(fileStream),';','"',1);
            final CSVParser parser = new CSVParserBuilder().withSeparator(';').build();
            final CSVReader reader =new CSVReaderBuilder(new InputStreamReader(fileStream)).withSkipLines(1).withCSVParser(parser).withFieldAsNull(CSVReaderNullFieldIndicator.BOTH).build();
            String[] line;
            setUser(UsuarioDao.obterUsuarioId(usuarioId));
            inventario = new Inventario();
        
        dt = new java.util.Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
        String currentTime = sdf.format(dt.getTime());
        try {
            dt = (Date)sdf.parse(currentTime);
        } catch (ParseException ex) {
            Logger.getLogger(UploadAction.class.getName()).log(Level.SEVERE, null, ex);
        }
            flag = true;
             while ((line = reader.readNext()) != null) {         
               //System.out.println(user.getNome());
               insert(line[0],line[1],Integer.parseInt(line[3]),Double.parseDouble(line[4].replace(',', '.')),Double.parseDouble(line[5].replace(',', '.')),line[8],Double.parseDouble(line[7].replace(',', '.')),line[2],Float.parseFloat(line[6].replace(',', '.')));

            }
//          
        } catch (Exception e) {
            resp = "Erro ao ler o csv";
            e.printStackTrace();
            return ERROR;
            
        }

        return SUCCESS;
    }
    
    
    public String insert(String nomeCientifico,String nomepop, int numIndividuos,Double frequencia,Double areaBasal,String numUsos,Double biomassa,String familia,float valorComercial) {
        try{
        if (flag == true){
        inventario.setData(dt);
        System.out.println("setdata");
        inventario.setUsuario(user);
        System.out.println("setuser");
        InventarioDao.insert(inventario);
        System.out.println("insertinv");
        flag = false;
        }
        especies = EspecieDao.obterPorNomeCientifico(nomeCientifico);
        especieRegiao = EspecieRegiaoDao.obterPorEspecie(especies);
        System.out.println("1");
        if(especies == null){
        especies = new Especies();
        especies.setNomeCientifico(nomeCientifico);
        if ("" == numUsos){
        } else {
            especies.setNumeroUsos(Integer.parseInt(numUsos));
        }
        
        especies.setFamilia(familia);
        EspecieDao.insert(especies);
        System.out.println("2");
        }
        //System.out.println("Idespeciedao"+especies.getEspecieId());
        //especieRegiao = EspecieRegiaoDao.obterPorNomePop(nomepop);
        if (especieRegiao == null){
        especieRegiao = new EspecieRegiao();
        especieRegiao.setEspecies(EspecieDao.obterPorId(especies.getEspecieId()));
        especieRegiao.setRegioes(RegioesDao.obterPorId(1));
        especieRegiao.setNomePopular(nomepop);
        especieRegiao.setValorComercial(valorComercial);
        EspecieRegiaoDao.insert(especieRegiao);
            System.out.println("3");
        }
             
        System.out.println("4");
        //EspecieRegiao especieregiao = EspecieRegiaoDao.obterPorNomePop(nomepop);
        setEspecieInventario(new EspecieInventario());
        System.out.println("5");
        setEspecieInventarioid(new EspecieInventarioId());
        System.out.println("6");
        //List<Inventario> inventariolist = InventarioDao.obterPorUsuario(user);
        especieInventarioid.setEspecieRegiaoEspecieRegiaoId(especieRegiao.getEspecieRegiaoId());
        System.out.println("7");
        especieInventarioid.setInventarioInventarioId(inventario.getInventarioId());
        System.out.println("8");
        especieInventario.setId(especieInventarioid);
        System.out.println("9");
        //especieInventario.setEspecieRegiao(especieRegiao);
        //especieInventario.setInventario(inventario);
        especieInventario.setAreaBasal(areaBasal);
        especieInventario.setBiomassa(biomassa);
        especieInventario.setFrequencia(frequencia);
        especieInventario.setNumeroIndividuos(numIndividuos);
        //especieInventario.setVolume(volume);
        //System.out.println("uploadinsert");
        

        EspecieInventarioDao.insert(especieInventario);
        resp = "Inventário salvo com Sucesso"; 
        } catch(Exception e) {
            resp = "erro ao inserir no banco";
            return ERROR;
        }

   
       

        return SUCCESS;
    }
    

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file) {
        this.file = file;
    }


    /**
     * @param especieInventario the especieInventario to set
     */
    public void setEspecieInventario(EspecieInventario especieInventario) {
        this.especieInventario = especieInventario;
    }


    /**
     * @param especieInventarioid the especieInventarioid to set
     */
    public void setEspecieInventarioid(EspecieInventarioId especieInventarioid) {
        this.especieInventarioid = especieInventarioid;
    }


    /**
     * @param user the user to set
     */
    public void setUser(Usuario user) {
        this.user = user;
    }


    public String getResp() {
        return resp;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
    
    
}

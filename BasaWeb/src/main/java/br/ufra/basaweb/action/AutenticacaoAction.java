/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.action;

import br.ufra.basaweb.dao.UsuarioDao;
import br.ufra.basaweb.model.NivelAcesso;
import br.ufra.basaweb.model.Usuario;
import br.ufra.basaweb.util.EmailServive;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ActionSupport;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author Marcus
 */
@Namespace("/Autenticacao")
@Result(name = "success", type = "json")
@ParentPackage("json-default")
public class AutenticacaoAction extends ActionSupport {
    
    
    private String nome;
    private String email;
    private String senha;
    
    private int idUsuario;
    private String novaSenha;

    private HashMap<String, String> data;
    private int code;
    private String msg;
    
     @Action("entrar")
    public String entrar() {
        data = new HashMap<>();
        try {
            Usuario usuario = UsuarioDao.obterPorEmail(email);
            if (usuario != null) {
                String senhaCrip;
                MessageDigest m = MessageDigest.getInstance("MD5");
                m.update(this.senha.getBytes(), 0, this.senha.length());
                senhaCrip = new BigInteger(1, m.digest()).toString(16);
                if (usuario.getSenha().equals(senhaCrip)) {
                    code = 0;
                    data.put("idUsuario", "" + usuario.getUsuarioId());
                    data.put("email", "" + usuario.getLogin());
                    nome = usuario.getNome();
                    msg = "Sucesso";
                } else {
                    code = 10;
                    msg = "Senha inválida";
                }
            } else {
                code = 11;
                msg = "Usuário inexistente";
            }
        } catch (Exception ex) {
            String msg = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()) + "\n"
                    + "Erro ao executar metodo entrar() na Class Autenticacao\n"
                    + Arrays.toString(ex.getStackTrace()).replaceAll(",", "\n");
          
            code = 999;
            this.msg = "Houve um problema durante a execução da rotina. Um notificação ja foi enviada para os administradores do sistema";
        }
        return SUCCESS;
    }
    
    @Action("recuperarSenha")
    public String recuperarSenha(){
        System.out.println("Nova senha: "+novaSenha);
        try{
            
            Usuario usuario = UsuarioDao.obterPorEmail(email);
            Usuario usr = UsuarioDao.obterUsuarioId(usuario.getUsuarioId());
            if(usuario == null){
                code = 11;
                msg = "Email não cadastrado";
            }
            else{
                code = 0;
                String senhaCrip2;
                MessageDigest m = MessageDigest.getInstance("MD5");
                m.update(this.novaSenha.getBytes(), 0, this.novaSenha.length());
                senhaCrip2 = new BigInteger(1, m.digest()).toString(16);
                usr.setSenha(senhaCrip2);
                usr.setNome(usuario.getNome());
                usr.setLogin(email);
                
                usr.setNivelAcesso(NivelAcessoDao.obterPorId(1));
                UsuarioDao.salvar(usr);
                msg = "Sucesso";

                
                EmailServive mail = new EmailServive();

                String men = "Olá " + usuario.getNome() + "," + "\tvocê alterou a sua senha.";

                mail.sendEmail(usuario.getLogin(), "Restaura - Nova Senha", men);
                    
            }
        }catch(Exception ex){
            System.out.println(ex.getStackTrace());
            
        }
        return SUCCESS;
    }
    

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the idUsuario
     */
    public int getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the novaSenha
     */
    public String getNovaSenha() {
        return novaSenha;
    }

    /**
     * @param novaSenha the novaSenha to set
     */
    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    /**
     * @return the data
     */
    public HashMap<String, String> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(HashMap<String, String> data) {
        this.data = data;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.action;

import br.ufra.basaweb.model.Usuario;
import br.ufra.basaweb.util.Email;
import br.ufra.basaweb.util.EmailServive;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Calendar;
import java.util.HashMap;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author Beatriz
 */
@Namespace("/Notificacao")
@Result(name = "success", type = "json")
@ParentPackage("json-default")
public class NotificacaoAction extends ActionSupport{
    private static final long serialVersionUID = -6765991741441442190L;
    private int code;
    private String msg;
    private HashMap<String,String> data;
    
    private long idUsuario;
    private long timestamp;
    private String stacktrace;
    private String email;
    private String nome;
    
    @Action("email")
    public String email(){
        String mensagem;
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(getTimestamp());
        String datetime = c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + c.get(Calendar.DAY_OF_MONTH) + " " + c.get(Calendar.HOUR_OF_DAY) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND);
        mensagem = this.getIdUsuario() +" - UsuarioTeste\n"+getMsg()+"\n"+datetime+"\n"+getStacktrace();
        Email mail = new Email(mensagem);
        return SUCCESS;
    }
    @Action("contato")
    public String contato(){
        String mensagem;
        
        mensagem = "Nome: "+ nome+ "<br />Email do usuário: "+ email + "<br />Mensagem: " + msg;
        
        EmailServive mail = new EmailServive();
        mail.sendEmailContato("projetoufrabasa@gmail.com", "Restaura Floresta - Contato", mensagem);
        
        System.out.println("email enviado com sucesso");
        
        return SUCCESS;
    }

    /**
     * @return the code
     */
    public int getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the data
     */
    public HashMap<String,String> getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(HashMap<String,String> data) {
        this.data = data;
    }

    /**
     * @return the idUsuario
     */
    public long getIdUsuario() {
        return idUsuario;
    }

    /**
     * @param idUsuario the idUsuario to set
     */
    public void setIdUsuario(long idUsuario) {
        this.idUsuario = idUsuario;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the stacktrace
     */
    public String getStacktrace() {
        return stacktrace;
    }

    /**
     * @param stacktrace the stacktrace to set
     */
    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    } 
    
}
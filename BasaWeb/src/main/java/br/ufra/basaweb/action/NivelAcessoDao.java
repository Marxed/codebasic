/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.action;

import br.ufra.basaweb.model.NivelAcesso;
import br.ufra.basaweb.util.HibernateUtil;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Marcus 
 * 31/08
 */
public class NivelAcessoDao {

    public static NivelAcesso obterPorId(Integer id) {
        Session session = HibernateUtil.getSession();
        Criteria criteria = session.createCriteria(NivelAcesso.class);
        criteria.add(Restrictions.eq("nivelAcessoId", id));
        List<NivelAcesso> nivelAcessos = criteria.list();
        //session.close();
        if (nivelAcessos.isEmpty()) {
            NivelAcesso nivelAcesso = new NivelAcesso();
            nivelAcesso.setNivelAcessoId(1);
            nivelAcesso.setNivel("1");
            session.save(nivelAcesso);
            //session.close();
            return nivelAcesso;
        }
        return nivelAcessos.get(0);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.action;

import br.ufra.basaweb.dao.EspecieInventarioDao;
import br.ufra.basaweb.dao.EspecieInventarioIdDao;
import br.ufra.basaweb.dao.InventarioDao;
import br.ufra.basaweb.dao.UsuarioDao;
import br.ufra.basaweb.model.EspecieInventario;
import br.ufra.basaweb.model.EspecieInventarioId;
import br.ufra.basaweb.model.Inventario;
import br.ufra.basaweb.model.TabelaInventarios;
import br.ufra.basaweb.model.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author Ivso
 */
@Namespace("/InventarioSelecao")
@Result(name = "success", type = "json")
@ParentPackage("json-default")
public class InventariosAction extends ActionSupport{
    
    private int usuarioId;
    private int inventarioId;
    private List<TabelaInventarios> tabelaInventarios;
    private int[] especies;
    private List<Inventario> inventarios;
    
    @Action("consultar")
    public String consultar() {
        
        
        Usuario user = UsuarioDao.obterUsuarioId(usuarioId);
        
        inventarios = InventarioDao.obterPorUsuario(user);
        
        int c = inventarios.size();
        especies = new int[c];
        System.out.println(c);
        
        for (int i=0;i<c;i++){
            List<EspecieInventario> a = EspecieInventarioDao.obterPorInventario(inventarios.get(i));
            especies[i] = a.size();
        }
        tabelaInventarios = new ArrayList<>();
        int i=0;
        for(Inventario e : inventarios){
            tabelaInventarios.add(new TabelaInventarios(e,especies[i],i+1));
            i++;
        }
        
        Collections.reverse(tabelaInventarios);
       

        
        return SUCCESS;
    }
    
    @Action("delete")
    public String delete() {
        //List<EspecieInventarioId> ids = EspecieInventarioIdDao.obterPorInventarioId(inventarioId);
        System.out.println("dele start");
        //List<EspecieInventarioId> eii = EspecieInventarioIdDao.obterPorInventario(inventarioId);
       Inventario inventa = new Inventario();
       inventa.setInventarioId(inventarioId);
        List<EspecieInventario> especs= EspecieInventarioDao.obterPorInventario(inventa);
        System.out.println(especs.size());
        EspecieInventarioDao.delete(especs);
        InventarioDao.delete(inventa);
        System.out.println("delete finish");

        return SUCCESS;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public List<TabelaInventarios> getTabelaInventarios() {
        return tabelaInventarios;
    }

    public void setTabelaInventarios(List<TabelaInventarios> tabelaInventarios) {
        this.tabelaInventarios = tabelaInventarios;
    } 

    public int getInventarioId() {
        return inventarioId;
    }

    public void setInventarioId(int inventarioId) {
        this.inventarioId = inventarioId;
    }
    
    
    
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufra.basaweb.action;


import br.ufra.basaweb.dao.InventarioDao;
import br.ufra.basaweb.dao.UsuarioDao;
import br.ufra.basaweb.model.Inventario;
import br.ufra.basaweb.model.Usuario;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author Ivso
 */
@Namespace("/DashboardAction")
@Result(name = "success", type = "json")
@ParentPackage("json-default")
public class DashboardAction extends ActionSupport {
    private int numInventario;
    private int numUsuarios;
    private int usuarioId;
    
    @Action("returninv")
    public String numInv(){
        Usuario user = new Usuario();
        user.setUsuarioId(usuarioId);
        List<Inventario> inventarios = InventarioDao.obterPorUsuario(user);
        if(inventarios == null){
            numInventario = 0;
        } else {
        numInventario = inventarios.size();
        }
        return SUCCESS;
}
    
    @Action("returnusers")
    public String numUsuarios(){
        List<Usuario> usuarios = UsuarioDao.listarlazy();
        if(usuarios == null){
            numUsuarios = 0;
        } else {
        numUsuarios = usuarios.size();
        }
        return SUCCESS;
    }

    public int getNumInventario() {
        return numInventario;
    }

    public int getNumUsuarios() {
        return numUsuarios;
    }
    
    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
    
    
    
}

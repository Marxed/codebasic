##Versão 3

args = commandArgs(trailingOnly=TRUE)
rootPath <- args[1]
fileInventario <- "inventa.csv"
caminho <- paste(rootPath,fileInventario,sep = "")


inventario <- read.csv2(caminho, header = TRUE, sep = ';', dec = ',', fileEncoding="latin1")
#apoio <- read.csv2('inventario.csv', header = TRUE, sep = ';', dec = ',')

qtd_linhas <- nrow(inventario) #qtd de linhas do arquivo .csv

tabela_completa = matrix(0, qtd_linhas, 9)  

tabela_completa = data.frame(tabela_completa)

col_headings <- c('nome_cientifico','nome_popular','familia','abr','fqr','dor','vcmr','biomr','usor')
names(tabela_completa) <- col_headings

tabela_completa[,1:3] <- inventario[,1:3]

#abr - densidade relativa¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

total_individuos = sum(inventario$num_individuos)#[1:qtd_linhas,4])

for (a in 1:qtd_linhas) {
  
  tabela_completa[a:qtd_linhas,4] = round((inventario[a,4]/total_individuos)*100, digits = 2)
  
}

#fqr - frequencia relativa¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

total_frequencia = sum(inventario$freq_absoluta)

for (f in 1:qtd_linhas) {
  
  tabela_completa[f:qtd_linhas,5] = round((inventario[f,5]/total_frequencia)*100, digits = 2)
  
}

#dor - dominancia relativa¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

total_area_basal = sum(inventario$area_basal)

for (d in 1:qtd_linhas){
  
  tabela_completa[d:qtd_linhas,6] = round((inventario[d,6]/total_area_basal)*100, digits = 2)
}  


#vcmr - valor comercial da madeira¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

valor_total = sum(inventario$valor_comercial)

for(v in 1:qtd_linhas){
  
  tabela_completa[v:qtd_linhas,7] = round((inventario[v,7]/valor_total)*100, digits = 2)
}

#biomr - biomassa¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

total_biom = sum(inventario$biomassa)

for (b in 1:qtd_linhas) {
  
  tabela_completa[b:qtd_linhas,8] = round( (inventario[b,8]/total_biom)*100, digits = 2)
  
}

#usor - usos¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨

total_usos = sum(inventario$usos)

for (t in 1:qtd_linhas) {
  
  tabela_completa[t:qtd_linhas,9] = round((inventario[t,9]/total_usos)*100, digits = 2)
  
}

#Script calculo IFSE
#autor: Beatriz Nery e Markus

#OBS: posteriormente será criada uma variável que repesente a criação de matriz, bem como a quantidade de
#suas linhas e colunas

##############1ªParte################tabelas##############################################
#carregar tabela (data set)                                                                         #
#tabela_completa = read.csv2('tabela_ifse.csv', header = TRUE, sep = ';', dec = ',')  

qtd_linhas <- nrow(tabela_completa) #qtd de linhas do arquivo .csv

qtd_colunas <- ncol(tabela_completa) #qtd de colunas


#criar tabela de 0's para posteriormente colocar o index. 
tabela_ranqueada_abr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_fqr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_dor = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_biomr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_vcmr = matrix(0, qtd_linhas, qtd_colunas)
tabela_ranqueada_usor = matrix(0, qtd_linhas, qtd_colunas)

tabela_dummy = matrix(0,qtd_linhas,1) #tabela para colocar as colunas com as variáveis dummy

#ordena todas as colunas a partir da coluna abr
index_abr<- with(tabela_completa, order(abr,fqr, dor, vcmr, biomr, usor, decreasing = TRUE))
tabela_ranqueada_abr =  tabela_completa[index_abr,]   
#ordena todas as colunas a partir de fqr
index_fqr <- with(tabela_completa, order(fqr, abr, dor, vcmr, biomr, usor, decreasing = TRUE))
tabela_ranqueada_fqr = tabela_completa[index_fqr,]
#ordena todas as colunas a partir da coluna dor
index_dor <- with(tabela_completa, order(dor, abr, fqr, vcmr, biomr, usor, decreasing = TRUE))
tabela_ranqueada_dor = tabela_completa[index_dor,]
#ordena todas as colunas a partir da coluna vcmr
index_vcmr <- with(tabela_completa, order(vcmr, abr, fqr, dor, biomr, usor, decreasing = TRUE))
tabela_ranqueada_vcmr = tabela_completa[index_vcmr,]
#ordena todas as colunas a partir da coluna biomr
index_biomr <- with(tabela_completa, order(biomr, abr, fqr, dor, vcmr, usor, decreasing = TRUE))
tabela_ranqueada_biomr = tabela_completa[index_biomr,]
#ordena 
index_usor <- with(tabela_completa, order(usor, abr, fqr, dor, vcmr, biomr, decreasing = TRUE))
tabela_ranqueada_usor = tabela_completa[index_usor,]

##############################Calculo ###############################################
t_individuos <- total_individuos# 36298 #nº total de indiviuos em mil

t_especies <- qtd_linhas #898 #n? total de especies

t_parcelas_especies <- 315 #total de parcelas de ocorrencia pra todas as especies

t_area_basal <- 1321 #area basal total

t_biomassa_especies <- total_biom  #26377324.25  #biomassa de todas as especies

num_plantar <- 1667

valor_total_comercial_madeira <- 415148.77 #vcmr total em R$ Obs: esse valor esta na tabela do spss(tabelão)
t_uso_especie <- total_usos #203 #usor da especie

############dummy#############
#Tabela que irá armazenar as variaveis e suas respectivas variaveis dummys.
tabela_completaD = matrix(0,qtd_linhas,12)

#abr#
for (i in qtd_linhas) {
  
  n_individuos_especie_abr <- ((tabela_ranqueada_abr$abr/100) * t_individuos) #
}

dim(n_individuos_especie_abr) <- c(qtd_linhas,1) #transforma um vetor em matriz

soma <- 0 #inicializa a variável soma com 0
x <- matrix(0,qtd_linhas,1) #cria um matriz de 0's com uma coluna e 898 linhas #obs.
#for para criacao das variaveis dummy
for (a in 1:qtd_linhas) {
  
  teste <- sum(n_individuos_especie_abr[a,]) #valor da linha da matriz
  soma <- soma+teste #soma as linhas da matriz
  
  if (soma <= (t_individuos/2)) {
    
    x[a,1] <- 1 #escrever variávei dummy na matriz
  }
}
index_F = index_abr;
tabela_apoio = matrix(0, qtd_linhas,2)
tabela_apoio[,1] = index_abr
tabela_apoio[,2] = x

caminhoCol <- "colDummy.R"
caminhoColD <- paste(rootPath,caminhoCol,sep ="")

source(caminhoColD)

tabela_completaD[,1] = tabela_completa[,4]
tabela_completaD[,2] = tabela_dummy


#fqr#
#pegar o numero de parcelas por especie
for(f in qtd_linhas){
  n_parcelas_especie_fqr <- ((tabela_ranqueada_fqr$fqr/100) * t_parcelas_especies) #obter o valor de n_parcelas
}

dim(n_parcelas_especie_fqr) <- c(qtd_linhas,1) #transforma um vetor em matriz

soma_fqr <- 0 #inicializa a variável com 0
x_fqr <- matrix(0, qtd_linhas, 1) #cria um matriz de 0's com uma coluna e 898 linhas #obs.
#for para somar fqr e escrever coluna 
for(fq in 1:qtd_linhas){
  teste_fqr <- sum(n_parcelas_especie_fqr[fq,]) #pegar valor de cada linha da matriz
  soma_fqr <- soma_fqr + teste_fqr #soma o valor das linhas da coluna
  if (soma_fqr <= (t_parcelas_especies/2)){
    #criar matriz p/ colocar dummy
    x_fqr[fq,1] <- 1 #escrever variávei dummy na matriz
  }
}

index_F = index_fqr;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_fqr
source(caminhoColD)
tabela_completaD[,3] = tabela_completa[,5]
tabela_completaD[,4] = tabela_dummy

#Area Basal ABr =  dor na tabela
for(dr in 1:qtd_linhas){
  
  area_basal <- (( tabela_ranqueada_dor$dor/100)*  t_area_basal)
  
}
dim(area_basal) <- c(qtd_linhas,1) # transforma um vetor em matriz

soma_area <- 0
x_area <- matrix(0,qtd_linhas,1) #cria uma matriz de 0's 
for (area in 1:qtd_linhas) {
  teste_area <- sum(area_basal[area,]) #pega o valor de cada linha da matriz 
  soma_area <- soma_area + teste_area #soma o valor das linhas da coluna
  
  if (soma_area <= (t_area_basal/2)) {
    x_area[area,1] <- 1 #escreve variável dummy na matriz
  }
  
}

index_F = index_dor;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_area
source(caminhoColD)
tabela_completaD[,5] = tabela_completa[,6]
tabela_completaD[,6] = tabela_dummy

#Valor comercial da madeira (vcmr)
for (vc in 1:qtd_linhas) {
  valor_comercial_madeira_especie <- ((tabela_ranqueada_vcmr$vcmr/100) * valor_total_comercial_madeira)
}
dim(valor_comercial_madeira_especie) <- c(qtd_linhas,1) #transforma um vetor em matriz

soma_vcmr <- 0
x_vcmr <- matrix(0, qtd_linhas, 1)
for (vcm in 1:qtd_linhas) {
  teste_vcmr <- sum(valor_comercial_madeira_especie[vcm,])
  soma_vcmr <- soma_vcmr + teste_vcmr
  
  if (soma_vcmr <= (valor_total_comercial_madeira/2)) {
    x_vcmr[vcm,1] <- 1
  }
}

index_F = index_vcmr;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_vcmr
source(caminhoColD)
tabela_completaD[,7] = tabela_completa[,7]
tabela_completaD[,8] = tabela_dummy

#########primerio corte

#biomassa
for (bio in 1:qtd_linhas) {
  biomassa_especie <- ((tabela_ranqueada_biomr$biomr/100) * t_biomassa_especies)
}
dim(biomassa_especie) <- c(qtd_linhas,1)

soma_bio <- 0
x_bio <- matrix(0,qtd_linhas,1)
for(biom in 1:qtd_linhas){
  teste_biom <- sum(biomassa_especie[biom,])
  soma_bio = soma_bio + teste_biom
  
  if(soma_bio <= (t_biomassa_especies/2)){
    x_bio[biom,1] <- 1
  }
}
index_F = index_biomr;
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_bio

source(caminhoColD)

tabela_completaD[,9] = tabela_completa[,8]
tabela_completaD[,10] = tabela_dummy
#PFNM
for (pf in 1:qtd_linhas) {
  pfmn_especie <- ((tabela_ranqueada_usor$usor/100) * t_uso_especie)
}

dim(pfmn_especie) <- c(qtd_linhas,1)

soma_pfnmr <- 0
x_pfnmr <- matrix(0, qtd_linhas, 1)
for (pfnm in 1:qtd_linhas) {
  teste_pfnm <- sum(pfmn_especie[pfnm,1])
  soma_pfnmr= soma_pfnmr + teste_pfnm
  
  if(soma_pfnmr <= (t_uso_especie/2)){
    x_pfnmr[pfnm,1] <- 1
    
  }
}
index_F = index_usor
tabela_apoio[,1] = index_F
tabela_apoio[,2] = x_pfnmr

source(caminhoColD)

tabela_completaD[,11] = tabela_completa[,9]
tabela_completaD[,12] = tabela_dummy
#-----------------------------------------------------------segundo corte

## varimax, testes de bartlett e kmo, pca
#Matriz de correlação entre as variáveis
correlacao <- cor(tabela_completaD)
##PCA
fito <- princomp(tabela_completaD, cor = TRUE) #gera os componentes principais
sumario <- summary(fito) #apresenta as variancias explicadas por cada componente principal, bem com a variancia acumulada
cargasF <- loadings(fito) #cargas fatoriais considerando todos os fatores

## terceiro corte

#plot(fito, type="lines") # quantidade de fatores suficientes à analise fatorial
testre <-fito$scores #calcula os escores das observações nas componentes principais
#biplot(fito) #mostra a angulação entre cada variável
#library(psych)
#library(GPArotation)
##Teste de Bartlett

#testeBar <- cortest.bartlett(tabela_completaD) #hipotese nula de que a matriz de correlação entre as variáveis são ñ correlacionáveis

##varimax
fito <- factanal(tabela_completaD, 3, rotation = "varimax", scores="none")
##print(fito, digits = 2, sort= TRUE)  ### tirar comentário pra poder ver o plot né...dã
load <- fito$loadings
##load #cargas fatoriais   -------------tirar comentário para pode ver o plot

# plot(load, type="p")
# text(load, labels = names(tabela_completaD), cex = 7) #adiciona o nome das variáveis

###fim cortes

##KMO
#Here is our data
Data <- data.frame(tabela_completaD)

#Run this function
kmo <- function(x)
{
  x <- subset(x, complete.cases(x)) # Omit missing values
  r <- cor(x) # Correlation matrix
  r2 <- r^2 # Squared correlation coefficients
  i <- solve(r) # Inverse matrix of correlation matrix
  d <- diag(i) # Diagonal elements of inverse matrix
  p2 <- (-i/sqrt(outer(d, d)))^2 # Squared partial correlation coefficients
  diag(r2) <- diag(p2) <- 0 # Delete diagonal elements
  KMO <- sum(r2)/(sum(r2)+sum(p2))
  MSA <- colSums(r2)/(colSums(r2)+colSums(p2))
return(list(KMO=KMO, MSA=MSA))
}
# 
#Run the test
#kmo(Data) ------tirar comentário pra poder ver o plot

#----------------Padroniza os scrores--------------
padroniza <- "padronizados.R"
caminhoPadro <- paste(rootPath,padroniza,sep = "")
source(caminhoPadro)

#-----------variancia-----------------------
va1 <-var(tabela_completaD[,1])
va2<-var(tabela_completaD[,2])
va3<-var(tabela_completaD[,3])
va4<-var(tabela_completaD[,4])
va5<-var(tabela_completaD[,5])
va6<-var(tabela_completaD[,6])
va7<-var(tabela_completaD[,7])
va8<-var(tabela_completaD[,8])
va9<-var(tabela_completaD[,9])
va10<-var(tabela_completaD[,10])
va11<-var(tabela_completaD[,11])
va12<-var(tabela_completaD[,12])

va <- as.vector(c(va1, va2, va3, va4, va5, va6, va7, va8, va9, va10, va11, va12))

#---------------------IFSE---------------------------------

sv <- sum(va)

tabelaIFSE <- matrix(0, qtd_linhas,13)
tabelaIFSE[,1:12] = tabela_completaD[,1:12]

for(s in 1:qtd_linhas){
  ifse = 0
for(e in 1:12){
  
  resul = (tabela_completaD[s,e]/27.37)*testre[s,e];
  ifse = resul + ifse
}
tabelaIFSE[s,13] = ifse
}

##Transforma em dataFrame
tabela_completaD <- as.data.frame.matrix(tabela_completaD)


##Dando nomes as colunas
names(tabela_completaD) <- c("abr", "abr-D", "fqr", "fqr-D", "dor", "dor-D", "vcmr", "vcmr-D", "biomr", "biomr-D", "usor", "usor-D")
 

tabela_completa$ifse <- ifelse( tabela_completaD$abr > 1, 1, 0)

tabela_completa[,10] = tabelaIFSE[,13]

index_ifse<- with(tabela_completa, order(ifse ,abr,fqr, dor, vcmr, biomr, usor, decreasing = TRUE))
tabela_completa =  tabela_completa[index_ifse,] 

tabela_completa$plantio <- 0

for (i in 1:qtd_linhas) {

  calculado = tabela_completa[i,4] * 1.1
  tabela_completa[i,11] = (1667 * calculado)/ 100

}

tabela_completa[1:qtd_linhas, 11] <- as.integer(tabela_completa[1:qtd_linhas, 11])

for (t in 1:qtd_linhas) {
  if (tabela_completa[t,11]< 10 && tabela_completa[t,11] != 0) {
    tabela_completa[t,11] = 10
    
  }
}

index_plantio <- with(tabela_completa, order(ifse,plantio ,usor, abr, fqr, dor, vcmr, biomr, decreasing = TRUE))
tabela_completa = tabela_completa[index_plantio,]

 nomeCientifico <- as.data.frame(tabela_completa$nome_cientifico)
 nomePopular <- as.data.frame(tabela_completa$nome_popular)
 indicefse <- as.data.frame(tabela_completa$ifse)
 indiceUsos <- as.data.frame(tabela_completa$usor)
 indicePlantio <- as.data.frame(tabela_completa$plantio)
 
 print(nomeCientifico, row.names = FALSE)
 print(nomePopular, row.names = FALSE)
 print(indiceUsos, row.names = FALSE)
 print(indicefse, row.names = FALSE)
 print(indicePlantio, row.names = FALSE)
